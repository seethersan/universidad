# -*- coding: utf-8 -*-
from setuptools import setup, find_packages
import os

version = '0.0.1'

setup(
    name='universidad',
    version=version,
    description='Modulo para la gestion de una Universidad',
    author='seethersan',
    author_email='carlos_jcez@hotmail.com',
    packages=find_packages(),
    zip_safe=False,
    include_package_data=True,
    install_requires=("frappe",),
)
