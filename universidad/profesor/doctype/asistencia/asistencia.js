// Copyright (c) 2016, seethersan and contributors
// For license information, please see license.txt
frappe.provide("universidad.asistencia");
frappe.ui.form.on('Asistencia', {
	refresh: function(frm) {
		frm.disable_save();
		frappe.call({
			method: "get_cursos_asistencia",
			doc: frm.doc,
			callback: function (r) {
				var curso = r.message;
				var cursos = [];
				for (var i in curso){
					cursos.push(curso[i]['curso'])
				}
				cur_frm.set_query("curso", function() {
			        return {
		        		"filters": [
			                ["curso", "in", cursos]
		            	]
		        	};
		    	});
			}
		})
		frappe.call({
			method: "get_today_date",
			doc: frm.doc,
			callback: function (r) {
				var today = r.message;
				frm.set_value("fecha", today)
			}
		})
	}
});


universidad.asistencia.check_mandatory_to_set_button = function(frm) {
	if (frm.doc.curso) {
		frm.fields_dict.registrar.$input.addClass("btn-primary");
	}
	else {
		frm.fields_dict.registrar.$input.removeClass("btn-primary");
	}
}


frappe.ui.form.on('Asistencia', 'curso', function(frm) {
	universidad.asistencia.check_mandatory_to_set_button(frm)
	frm.set_value("alumnos",[])
	if (frm.doc.curso) {
		frappe.call({
			method: "get_asistencia",
			doc: frm.doc,
			callback: function (r) {
				if (r.message) {
					frm.set_value("alumnos", r.message);
				}
				else {
					frappe.call({
						method: "get_alumnos_curso",
						doc: frm.doc,
						callback: function(r) {
							if(r.message) {
								frm.set_value("alumnos", r.message);
							}
						}
					});
				}
			}
		})
	}
	
});


frappe.ui.form.on('Asistencia', 'registrar', function(frm) {
	if (frm.doc.curso) {
		asistencia = frm.doc.alumnos
		if (frm.doc.registrado) {
			frappe.call({
				method: "delete_asistencia",
				doc: frm.doc,
				callback: function (r) {
					for (var i = 0; i < asistencia.length; i++) {
						frappe.call({
							method: "frappe.client.insert",
							args: {
								doc: {
									doctype: "Record de Asistencia",
									curso: frm.doc.curso,
									fecha: frm.doc.fecha,
									alumno: asistencia[i]["alumno"],
									asistio: asistencia[i]["asistio"]
								}
							}
						});
					};
				}
			})
			frm.set_value("registrado", true);
		}
		else {
			for (var i = 0; i < asistencia.length; i++) {
				frappe.call({
					method: "frappe.client.insert",
					args: {
						doc: {
							doctype: "Record de Asistencia",
							curso: frm.doc.curso,
							fecha: frm.doc.fecha,
							alumno: asistencia[i]["alumno"],
							asistio: asistencia[i]["asistio"]
						}
					}
				});
			};
			frm.set_value("registrado", true);
		}
	}
	else {
		frappe.throw("Curso no seleccionado")
	}
	
});
