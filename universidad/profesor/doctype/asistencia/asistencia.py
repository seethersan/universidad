# -*- coding: utf-8 -*-
# Copyright (c) 2015, seethersan and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe.model.document import Document
from universidad.universidad.utils import Utils

class Asistencia(Utils):
	def get_cantidad_clases(self):
		clases = frappe.db.sql("""select cantidad_clases as cantidad
			from `tabHorario Curso`
				where name = %(curso)s""",
			{
				"curso": self.curso
			}, as_dict=1)
		return clases[0]


	def get_asistencia(self):
		asistencia = frappe.db.sql("""select alumno as alumno, count(asistio) as faltas, format(count(asistio)/%(cantidad)s*100,2) as porcentaje
				from `tabRecord de Asistencia`
				where curso = %(curso)s
				and asistio = false
				and fecha <= %(fecha)s
				group by alumno""",
			{
				"curso": self.curso,
				"fecha": frappe.utils.today(),
				"cantidad": self.get_cantidad_clases()['cantidad']
			}, as_dict=1)
		return asistencia


	def delete_asistencia(self):
		frappe.db.sql("""delete from `tabRecord de Asistencia` where curso=%(curso)s
				and fecha=%(fecha)s""",
				{
					"curso": self.curso,
					"fecha": frappe.utils.today()
				})

