# -*- coding: utf-8 -*-
# Copyright (c) 2015, seethersan and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe.model.document import Document
import json
import requests
from datetime import datetime, date
from frappe import utils
import time

class Utils(Document):
	def get_carreras(self):
			carreras = frappe.db.sql("""select carrera as carrera 
					from `tabDetalle Carrera` where parent= %s """, 
					self.proceso_admision, as_dict=1)
			return carreras


	def get_alumno(self):
		alumno = frappe.db.sql("""select name as nombre
					from `tabAlumno` where nombre_completo = %s""",
					self.nombre, as_dict=1)
		return alumno


	def get_procesos(self):
		procesos = frappe.db.sql("""select codigo_proceso as codigo_proceso 
				from `tabProcesos de Admision` where fecha_examen <= %s """, 
				frappe.utils.today(), as_dict=1)
		return procesos


	def get_procesos_inscripcion(self):
		procesos = frappe.db.sql("""select codigo_proceso as codigo_proceso 
				from `tabProcesos de Admision` where fecha_inicio_registro <= %(date)s
				and fecha_fin_registro >= %(date)s""", 
				{
					"date": frappe.utils.today()
				}, as_dict=1)
		return procesos


	def get_postulantes(self):
		postulantes = frappe.db.sql("""select nombre_completo as postulante 
				from `tabPostulante` where proceso_admision= %s 
				and carrera= %s""", 
				(self.proceso_admision, self.carrera), as_dict=1)
		return postulantes


	def get_datos_postulante(self):
		postulante = frappe.db.sql("""select *
				from `tabPostulante` where nombre_completo = %s""",
				self.nombre, as_dict=1)
		return postulante


	def get_profesor(self):
		profesor = frappe.db.sql("""select employee_name as profesor_name 
				from `tabEmployee` where designation = 'Docente'""")
		return profesor


	def get_user(self):
		user = frappe.db.sql("""select name as name 
				from `tabEmployee` where user_id = %s""",
				frappe.session.user, as_dict=1)
		return user


	def get_cursos(self):
		user = self.get_user()
		name = user[0]['name']
		cursos = frappe.db.sql("""select curso as curso 
				from `tabHorario Curso` where profesor = %s""",
				name, as_dict=1)
		return cursos


	def get_datos_curso(self, name):
		curso = frappe.db.sql("""select curso as curso, profesor as profesor, periodo_academico as periodo
			from `tabHorario Curso` where name = %(name)s""",
			{
				"name": name
			}, as_dict=1)
		return curso[0]


	def get_cursos_asistencia(self):
		user = self.get_user()
		name = user[0]['name']
		fecha = frappe.utils.today()
		cursos_reprogramados = frappe.db.sql("""select cu.name as curso 
				from `tabHorario de Clases` cl, `tabHorario Curso` cu 
				where cu.curso = cl.curso
				and cu.periodo_academico = cl.periodo_academico
				and cu.profesor = cl.profesor
				and cl.profesor = %(profesor)s
				and cl.periodo_academico = %(periodo)s
				and cl.fecha_reprogramacion = %(fecha)s
				and cl.hora_inicio_reprogramacion < %(hora)s
				and cl.hora_fin_reprogramacion > %(hora)s
				and reprogramado = true""",
				{
					"hora": time.strftime("%H:%M:%S"),
					"profesor": name,
					"periodo": self.get_periodo()["periodo"],
					"fecha": fecha
				}, as_dict=1)
		if cursos_reprogramados == []:
			cursos = frappe.db.sql("""select cu.name as curso 
					from `tabHorario de Clases` cl, `tabHorario Curso` cu 
					where cu.curso = cl.curso
					and cu.periodo_academico = cl.periodo_academico
					and cu.profesor = cl.profesor
					and cl.profesor = %(profesor)s
					and cl.periodo_academico = %(periodo)s
					and cl.fecha = %(fecha)s
					and cl.hora_inicio < %(hora)s
					and cl.hora_salida > %(hora)s
					and reprogramado = false""",
					{
						"hora": time.strftime("%H:%M:%S"),
						"profesor": name,
						"periodo": self.get_periodo()["periodo"],
						"fecha": fecha
					}, as_dict=1)
		else:
			cursos = cursos_reprogramados
		return cursos


	def get_today_date(self):
		today = utils.today()
		return today


	def get_alumnos_curso(self):
		alumnos = frappe.db.sql("""select alumno as alumno, '0' as faltas, '0' as porcentaje 
				from `tabRegistro de Matricula` where curso = %s""",
				self.curso, as_dict=1)
		return alumnos


	def get_periodo(self):
		periodo = frappe.db.sql("""select codigo_periodo as periodo
			from `tabPeriodo Academico` where fecha_inicio <= %(date)s
			and fecha_fin >= %(date)s""", 
			{
				"date": frappe.utils.today()
			}, as_dict=1)
		return periodo[0]


	def get_horario(self):
		horarios = frappe.db.sql("""select IF(reprogramado,fecha_reprogramacion,fecha) as fecha
			from `tabHorario de Clases` where curso = %(curso)s
			and profesor = %(profesor)s
			and periodo_academico = %(periodo)s
			and fecha >= %(fecha)s""",
			{
				"curso": self.curso,
				"profesor": self.profesor,
				"periodo": self.get_periodo()['periodo'],
				"fecha": frappe.utils.today()
			}, as_dict=1)
		return horarios

	def get_horario_reprogramacion(self):
		horarios = frappe.db.sql("""select fecha as fecha
			from `tabHorario de Clases` where curso = %(curso)s
			and profesor = %(profesor)s
			and periodo_academico = %(periodo)s
			and fecha >= %(fecha)s""",
			{
				"curso": self.curso,
				"profesor": self.profesor,
				"periodo": self.get_periodo()['periodo'],
				"fecha": frappe.utils.today()
			}, as_dict=1)
		return horarios


# Copyright (c) 2015, Frappe Technologies and contributors
# For lice

class OverlapError(frappe.ValidationError): pass

def validate_overlap_for(doc, doctype, fieldname, value=None, day=None, from_time=None, to_time=None, periodo=None):
	"""Checks overlap for specified feild.
	
	:param fieldname: Checks Overlap for this feild 
	"""
	save = True
	
	existing = get_overlap_for(doc, doctype, fieldname, value, day, from_time, to_time, periodo)
	if existing:
		frappe.throw("{0} tiene conflicto en {1} por {2} {3}".format(doc.doctype, existing.name,
			doc.meta.get_label(fieldname) if not value else fieldname , value or doc.get(fieldname)), OverlapError)
		save = false
	return save
	
def get_overlap_for(doc, doctype, fieldname, value=None, day=None, from_time=None, to_time=None, periodo=None):
	"""Returns overlaping document for specified feild.
	
	:param fieldname: Checks Overlap for this feild 
	"""

	existing = frappe.db.sql("""select name, inicio_{2}, salida_{2} from `tab{0}`
		where `{1}`=%(val)s and `{2}` = True and
		(
			(inicio_{2} > %(from_time)s and inicio_{2} < %(to_time)s) or
			(salida_{2} > %(from_time)s and salida_{2} < %(to_time)s) or
			(%(from_time)s > inicio_{2} and %(from_time)s < salida_{2}) or
			(%(from_time)s = inicio_{2} and %(to_time)s = salida_{2}))
		and name!=%(name)s
		and periodo_academico = %(periodo)s""".format(doctype, fieldname, day),
		{
			"val": value or doc.get(fieldname),
			"from_time": from_time,
			"to_time": to_time,
			"name": doc.name or "No Name",
			"periodo": periodo
		}, as_dict=True)

	return existing[0] if existing else None


def validate_overlap_for_horario(doc, doctype, fieldname, value=None, day=None, from_time=None, to_time=None, periodo=None, cursos=None):
	"""Checks overlap for specified feild.
	
	:param fieldname: Checks Overlap for this feild 
	"""
	save = True
	
	existing = get_overlap_for_horario(doc, doctype, fieldname, value, day, from_time, to_time, periodo, cursos)
	if existing:
		frappe.throw("{0} tiene conflicto en {1} por {2} {3}".format(doc.doctype, existing.name,
			doc.meta.get_label(fieldname) if not value else fieldname , value or doc.get(fieldname)), OverlapError)
		save = false
	return save
	
def get_overlap_for_horario(doc, doctype, fieldname, value=None, day=None, from_time=None, to_time=None, periodo=None, cursos=None):
	"""Returns overlaping document for specified feild.
	
	:param fieldname: Checks Overlap for this feild 
	"""

	existing = frappe.db.sql("""select name, inicio_{2}, salida_{2} from `tab{0}`
		where `{1}`=%(val)s and `{2}` = True and
		(
			(inicio_{2} > %(from_time)s and inicio_{2} < %(to_time)s) or
			(salida_{2} > %(from_time)s and salida_{2} < %(to_time)s) or
			(%(from_time)s > inicio_{2} and %(from_time)s < salida_{2}) or
			(%(from_time)s = inicio_{2} and %(to_time)s = salida_{2}))
		and name!=%(name)s
		and periodo_academico = %(periodo)s
		and name in {3}""".format(doctype, fieldname, day, cursos),
		{
			"val": value or doc.get(fieldname),
			"from_time": from_time,
			"to_time": to_time,
			"name": doc.name or "No Name",
			"periodo": periodo
		}, as_dict=True)

	return existing[0] if existing else None
