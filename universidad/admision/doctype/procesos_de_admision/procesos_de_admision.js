// Copyright (c) 2016, seethersan and contributors
// For license information, please see license.txt
frappe.provide("universidad.procesos_de_admision");
frappe.ui.form.on('Procesos de Admision', {
	refresh: function(frm) {

	}
});


frappe.ui.form.on('Procesos de Admision', 'fecha_inicio', function(frm) {
	universidad.procesos_de_admision.check_mandatory_to_set_codigo(frm);	
});

frappe.ui.form.on('Procesos de Admision', 'fecha_fin', function(frm) {
	universidad.procesos_de_admision.check_mandatory_to_set_codigo(frm);	
});

frappe.ui.form.on('Procesos de Admision', 'fecha_examen', function(frm) {
	universidad.procesos_de_admision.check_mandatory_to_set_codigo(frm);	
});

universidad.procesos_de_admision.check_mandatory_to_set_codigo = function(frm) {
	if (frm.doc.fecha_inicio && frm.doc.fecha_fin && frm.doc.fecha_examen) {
		universidad.procesos_de_admision.get_codigo(frm);
	}
}


universidad.procesos_de_admision.get_codigo = function(frm) {
	if (frm.doc.fecha_inicio && frm.doc.fecha_fin && frm.doc.fecha_examen) {
		frappe.call({
				method: "set_codigo",
				doc: frm.doc,
				callback: function(r) {
					if(r.message) {
						frm.set_value("codigo_proceso", r.message);
					}
				}
			})
	}
}