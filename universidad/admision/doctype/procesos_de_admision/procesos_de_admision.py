# -*- coding: utf-8 -*-
# Copyright (c) 2015, seethersan and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe.model.document import Document
from datetime import datetime

class ProcesosdeAdmision(Document):
	def set_codigo(self):
		if not self.codigo_proceso:
			count = frappe.db.sql("""select (count(name) + 1) as cuenta
					from `tabProcesos de Admision` where YEAR(fecha_fin)= %s""",
					datetime.strptime(self.fecha_fin,"%Y-%m-%d").year)
			cuenta = count[0]
			codigo = str(datetime.strptime(self.fecha_fin,"%Y-%m-%d").year) + str(cuenta[0]).zfill(2)
		else:
			codigo = self.codigo_proceso
		return codigo
