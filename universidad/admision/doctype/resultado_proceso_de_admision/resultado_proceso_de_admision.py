# -*- coding: utf-8 -*-
# Copyright (c) 2015, seethersan and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe.model.document import Document
from universidad.universidad.utils import Utils

class ResultadoProcesodeAdmision(Utils):
	def get_resultado_admision(self):
		"""Checks overlap for specified feild.
		
		:param fieldname: Checks Overlap for this feild 
		"""
		notas = []
		admision = get_resultados("Examen de Admision", self.proceso_admision,  self.carrera)
		entrevista = get_resultados("Entrevista", self.proceso_admision, self.carrera)
		psicologico = get_resultados("Evaluacion Psicologica", self.proceso_admision, self.carrera)
		mensaje = "No se encuentra "
		if not admision or not entrevista or not psicologico:
			if not admision:
				mensaje = mensaje + "Resultado de Examen de Admisión "
			if not entrevista:
				mensaje = mensaje + "Resultado de Entrevista "
			if not psicologico:
				mensaje = mensaje + "Resultado de Evaluación Psicológica"
			frappe.throw(mensaje + " del Periodo " + self.proceso_admision)
		else:
			notas = get_notas(self.proceso_admision + " - " + self.carrera)
			print(notas)
		return notas


def get_resultados(doctype, proceso=None, carrera=None):
	"""Returns overlaping document for specified feild.
	
	:param fieldname: Checks Overlap for this feild 
	"""

	existing = frappe.db.sql("""select name from `tab{0}`
		where `proceso_admision`=%(proceso)s
		and `carrera`=%(carrera)s""".format(doctype),
		{
			"proceso": proceso,
			"carrera": carrera
		}, as_dict=True)

	return existing[0] if existing else None


def get_notas(proceso=None):
	"""Returns overlaping document for specified feild.
	
	:param fieldname: Checks Overlap for this feild 
	"""

	notas = frappe.db.sql("""select eva.postulante as postulante,
	    (eva.nota_evaluacion_psicologico + ent.nota_entrevista + exa.nota_examen_admision)/3 as nota_final,
	    IF((eva.nota_evaluacion_psicologico + ent.nota_entrevista + exa.nota_examen_admision)/3>10.5,'INGRESO','NO INGRESO') as estado
	    from `tabDetalle Evaluacion Psicologica` as eva,
		`tabDetalle Entrevista` as ent,
		`tabDetalle Examen Admision` as exa
		where ent.parent = eva.parent
		and eva.parent = exa.parent
		and exa.parent = %(proceso)s
		and eva.postulante = exa.postulante
		and exa.postulante = ent.postulante""",
		{
			"proceso": proceso,
		}, as_dict=True)

	return notas
	