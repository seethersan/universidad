// Copyright (c) 2016, seethersan and contributors
// For license information, please see license.txt
frappe.provide("universidad.examen_de_admision");
frappe.ui.form.on('Examen de Admision', {
	refresh: function(frm) {
		frappe.call({
			method: "get_procesos",
			doc: frm.doc,
			callback: function (r) {
				var proceso = r.message;
				var procesos = [];
				for (var i in proceso){
					procesos.push(proceso[i]['codigo_proceso'])
				}
				cur_frm.set_query("proceso_admision", function() {
			        return {
		        		"filters": [
			                ["name", "in", procesos]
		            	]
		        	};
		    	});
			}
		})
	}
});


frappe.ui.form.on('Examen de Admision', 'proceso_admision', 
	function(frm) {
		frappe.call({
			method: "get_carreras",
			doc: frm.doc,
			callback: function (r) {
				var carrera = r.message;
				var carreras = [];
				for (var i in carrera){
					carreras.push(carrera[i]['carrera'])
				}
				cur_frm.set_query("carrera", function() {
			        return {
		        		"filters": [
			                ["name", "in", carreras]
		            	]
		        };
		    });
		}
	})
});


frappe.ui.form.on('Examen de Admision', 'carrera', 
	function(frm) {
		frm.set_value("notas",[])
		frm.set_value("codigo", frm.doc.proceso_admision + " - " + frm.doc.carrera)
		frappe.call({
			method: "get_postulantes",
			doc: frm.doc,
			callback: function(r) {
				if(r.message) {
					frm.set_value("notas", r.message);
				}
		}
	});
});

