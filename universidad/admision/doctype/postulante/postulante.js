// Copyright (c) 2016, seethersan and contributors
// For license information, please see license.txt
frappe.provide("universidad.postulante");
frappe.ui.form.on('Postulante', {
	refresh: function(frm) {
		frappe.call({
			method: "get_procesos",
			doc: frm.doc,
			callback: function (r) {
				var proceso = r.message;
				var procesos = [];
				for (var i in proceso){
					procesos.push(proceso[i]['codigo_proceso'])
				}
				cur_frm.set_query("proceso_admision", function() {
			        return {
		        		"filters": [
			                ["name", "in", procesos]
		            	]
		        	};
		    	});
			}
		})
	}
});


frappe.ui.form.on('Postulante', {
	validate: function(frm) {
	var today = new Date();		
		frm.set_value("estado", "Postulante Pre-Inscrito");
		frappe.call({
			method: "get_customer",
			doc: frm.doc,
			callback: function (r) {
				console.log(r.message)
				if (r.message) {

				}
				else {
					frappe.call({
						method: "frappe.client.insert",
						args: {
							doc: {
								doctype: "Customer",
								customer_name: frm.doc.nombre_completo,
								categoria: "Postulante Pre-Inscrito",
								codigo: frm.doc.codigo_postulante,
								customer_type: "Individual",
								customer_group: "Individual",
								territory: "Peru",
								tipo_documento_identidad: frm.doc.tipo_documento,
								tax_id: frm.doc.numero_documento,
								codigo_tipo_documento: frm.doc.codigo_tipo_documento
							}
						}
					});
				}
			}
		});
		frappe.call({
			method: "get_sales_order",
			doc: frm.doc,
			callback: function (r) {
				if (r.message) {

				}
				else {
					frappe.call({
						method: "frappe.client.insert",
						args: {
							doc: {
								doctype: "Sales Order",
								customer: frm.doc.nombre_completo,
								tax_id: frm.doc.numero_documento,
								codigo_tipo_documento: frm.doc.codigo_tipo_documento,
								delivery_date: today,
								order_type: "Sales",
								docstatus: 1,
								status: "To Deliver and Bill",
								items: [{
									item_code: "Inscripción Examen de Admisión",
									item_name: "Inscripción Examen de Admisión",
									description: "Inscripción Examen de Admisión",
									qty: 1,
									rate: frm.doc.costo_examen
								}]
							}
						}
					})
				}
			}
		});
		if(frm.doc.requisitos){
			var values = frm.doc.requisitos
			frm.set_value("requisitos", values)
		}
	}
});


frappe.ui.form.on("Postulante", {
	tipo_documento: function(frm, cdt, cdn) {
		var tipo_documento = frappe.model.get_doc(cdt, cdn);
		if (tipo_documento.tipo_documento){
			frappe.call({
				type: "GET",
				method: "ple.ple_peru.doctype.tipos_de_documento_de_identidad.tipos_de_documento_de_identidad.get_tipo_documento",
				args: {
					tipo_documento_identidad: tipo_documento.tipo_documento
				},
				callback: function(r) {
					frappe.model.set_value(cdt, cdn, "codigo_tipo_documento", r.message);
				}
			});
		}
		else {
			frappe.model.set_value(cdt, cdn, "codigo_tipo_documento", null);
		}
	}
});


frappe.ui.form.on('Postulante', 'apellido_paterno', function(frm) {
	universidad.postulante.check_mandatory_to_set_nombre_completo(frm);
});


frappe.ui.form.on('Postulante', 'apellido_materno', function(frm) {
	universidad.postulante.check_mandatory_to_set_nombre_completo(frm);
});


frappe.ui.form.on('Postulante', 'nombres', function(frm) {
	universidad.postulante.check_mandatory_to_set_nombre_completo(frm);
});


frappe.ui.form.on('Postulante', 'proceso_admision', 
	function(frm) {
		universidad.postulante.check_mandatory_to_set_codigo(frm);
		frappe.call({
			method: "get_carreras",
			doc: frm.doc,
			callback: function (r) {
				var carrera = r.message;
				var carreras = [];
				for (var i in carrera){
					carreras.push(carrera[i]['carrera'])
				}
				cur_frm.set_query("carrera", function() {
			        return {
		        		"filters": [
			                ["name", "in", carreras]
		            	]
		        };
		    });
		}
	})
});


frappe.ui.form.on('Postulante', 'carrera', 
	function(frm) {
		frm.set_value("requisitos",[])
		frappe.call({
			method: "get_requisitos",
			doc: frm.doc,
			callback: function(r) {
				if(r.message) {
					frm.set_value("requisitos", r.message);
				}
			}
		});
		frappe.call({
			method: "get_costo",
			doc: frm.doc,
			callback: function (r) {
				if(r.message) {
					var costo = r.message[0]
					frm.set_value("costo_examen", costo['admision']);
				}				
			}
		})
});


universidad.postulante.check_mandatory_to_set_codigo = function(frm) {
	if (frm.doc.proceso_admision) {
		universidad.postulante.get_codigo(frm);
	}
}

universidad.postulante.check_mandatory_to_set_nombre_completo = function(frm) {
	if (frm.doc.apellido_paterno && frm.doc.apellido_materno && frm.doc.nombres) {
		universidad.postulante.set_nombre_completo(frm);
	}
}

universidad.postulante.get_codigo = function(frm) {
	if (frm.doc.proceso_admision) {
		frappe.call({
				method: "set_codigo",
				doc: frm.doc,
				callback: function(r) {
					if(r.message) {
						frm.set_value("codigo_postulante", r.message);
					}
				}
			})
	}
}

universidad.postulante.set_nombre_completo = function(frm) {
	if (frm.doc.apellido_paterno && frm.doc.apellido_materno && frm.doc.nombres) {
		frm.set_value("nombre_completo", frm.doc.apellido_paterno + " " + frm.doc.apellido_materno + " " + frm.doc.nombres)
	}
}

