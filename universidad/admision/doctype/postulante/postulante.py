# -*- coding: utf-8 -*-
# Copyright (c) 2015, seethersan and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe.model.document import Document
import json
import requests
from datetime import datetime, date
from universidad.universidad.utils import Utils

class Postulante(Utils):
	def get_requisitos(self):
		requisitos = frappe.db.sql("""select nombre_requisito as requisito 
				from `tabRequisitos Carreras` where parent= %s """, 
				self.carrera, as_dict=1)
		return requisitos


	def get_customer(self):
		cliente = frappe.db.sql("""select name as name 
				from `tabCustomer` where codigo = %s """, 
				self.codigo_postulante, as_dict=1)
		return cliente


	def get_costo(self):
		costo = frappe.db.sql("""select admision as admision
				from `tabCostos de Carrera` where carrera = %s
				order by creation desc limit 1""", 
				self.carrera, as_dict=1)
		return costo


	def get_sales_order(self):
		sales_order = frappe.db.sql("""select sales.name as name 
				from `tabSales Order` sales,
				`tabSales Order Item` item 
				where item.parent = sales.name
				AND sales.tax_id = %s
				AND item.item_name = %s """, 
				(self.numero_documento, "Inscripción Examen de Admisión"), as_dict=1)
		return sales_order


	def set_codigo(self):
		if not self.codigo_postulante:
			count = frappe.db.sql("""select (count(name) + 1) as cuenta
					from `tabPostulante` where YEAR(creation)= %s""",
					date.today().year)
			cuenta = count[0]
			proceso = self.proceso_admision
			codigo = proceso + str(cuenta[0]).zfill(4)
		else:
			codigo = self.codigo_postulante 
		return codigo

