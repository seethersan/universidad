# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from frappe import _

def get_data():
	return [
		{
			"label": _("Intranet"),
			"icon": "icon-star",
			"items": [
				{
					"type": "doctype",
					"name": "Constancia de Notas",
					"description": _("Constancia de Notas."),
				}
			]
		}
	]
