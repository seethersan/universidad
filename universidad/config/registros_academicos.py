# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from frappe import _

def get_data():
	return [
		{
			"label": _("Matrícula"),
			"icon": "icon-star",
			"items": [
				{
					"type": "doctype",
					"name": "Matricula",
					"description": _("Registro de Matriula."),
				},
				{
					"type": "doctype",
					"name": "Rectificacion de Matricula",
					"description": _("Rectificacion de Matricula."),
				},
				{
					"type": "doctype",
					"name": "Retiro de Ciclo",
					"description": _("Retiro de Ciclo."),
				},
				{
					"type": "doctype",
					"name": "Retiro de Curso",
					"description": _("Retiro de Curso."),
				}
			]
		},
		{
			"label": _("Alumno"),
			"icon": "icon-star",
			"items": [
				{
					"type": "doctype",
					"name": "Inscripcion de Ingresantes",
					"description": _("Inscripcion de Ingresantes."),
				},
				{
					"type": "doctype",
					"name": "Alumno",
					"description": _("Alumno."),
				}
			]
		},
		{
			"label": _("Documentos"),
			"icon": "icon-cog",
			"items": [
				{
					"type": "doctype",
					"name": "Certificado de Estudios",
					"description": _("Certificado de Estudios.")
				},
				{
					"type": "doctype",
					"name": "Consulta de Notas",
					"description": _("Consulta de Notas.")
				}
			]
		}
	]
