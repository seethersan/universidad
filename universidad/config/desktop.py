# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from frappe import _

def get_data():
	return [
		{
			"module_name": "Intranet",
			"color": "green",
			"icon": "octicon octicon-person",
			"type": "module",
			"label": _("Intranet")
		},
		{
			"module_name": "Admision",
			"color": "red",
			"icon": "octicon octicon-clippy",
			"type": "module",
			"label": _("Admision")
		},
		{
			"module_name": "Registros Academicos",
			"color": "green",
			"icon": "icon-file-text-alt",
			"type": "module",
			"label": _("Registros Academicos")
		},
		{
			"module_name": "Direccion Academica",
			"color": "brown",
			"icon": "icon-map-marker",
			"type": "module",
			"label": _("Direccion Academica")
		},
		{
			"module_name": "Profesor",
			"color": "blue",
			"icon": "octicon octicon-broadcast",
			"type": "module",
			"label": _("Profesor")
		},		
	]
