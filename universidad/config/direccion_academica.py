# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from frappe import _

def get_data():
	return [
		{
			"label": _("Programación Académica"),
			"icon": "icon-star",
			"items": [
				{
					"type": "doctype",
					"name": "Periodo Academico",
					"description": _("Registro de Periodo Academico."),
				},
				{
					"type": "doctype",
					"name": "Horario",
					"description": _("Registro de Horario."),
				},
				{
					"type": "doctype",
					"name": "Programacion de Clases",
					"description": _("Registro de Programacion de Clases."),
				},
				{
					"type": "doctype",
					"name": "Reprogramacion de Clases",
					"description": _("Registro de Reprogramacion de Clases."),
				}
			]
		},
		{
			"label": _("Asistencia"),
			"icon": "icon-star",
			"items": [
				{
					"type": "doctype",
					"name": "Registro de Asistencia Profesor",
					"description": _("Registro de Asitencia de los profesores."),
				},
				{
					"type": "doctype",
					"name": "Justificacion de Inasistencia Profesor",
					"description": _("Rustificacion de Inasitencia Profesor."),
				}
			]
		},
		{
			"label": _("Dirección Académica"),
			"icon": "icon-cog",
			"items": [
				{
					"type": "doctype",
					"name": "Facultad",
					"description": _("Registro de Facultades.")
				},
				{
					"type": "doctype",
					"name": "Carrera",
					"description": _("Registro de Carreras.")
				},
				{
					"type": "doctype",
					"name": "Ciclo",
					"description": _("Registro de Ciclo.")
				},
				{
					"type": "doctype",
					"name": "Cursos",
					"description": _("Registro de Curso.")
				},
				{
					"type": "doctype",
					"name": "Costos de Carrera",
					"description": _("Registro de Costos de Carrera.")
				}
			]
		}
	]
