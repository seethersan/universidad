# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from frappe import _

def get_data():
	return [
		{
			"label": _("Asistencia"),
			"icon": "icon-star",
			"items": [
				{
					"type": "doctype",
					"name": "Asistencia",
					"description": _("Registro de Asistencia."),
				}
			]
		},
		{
			"label": _("Registro de Notas"),
			"icon": "icon-star",
			"items": [
				{
					"type": "doctype",
					"name": "Registro de Notas Periodo 1",
					"description": _("Registro de Notas Periodo 1."),
				},
				{
					"type": "doctype",
					"name": "Registro de Notas Examen Parcial",
					"description": _("Registro de Notas Examen Parcial."),
				},
				{
					"type": "doctype",
					"name": "Registro de Notas Periodo 2",
					"description": _("Registro de Notas Periodo 2."),
				},
				{
					"type": "doctype",
					"name": "Registro de Notas Examen Final",
					"description": _("Registro de Notas Examen Final."),
				}
			]
		}
	]
