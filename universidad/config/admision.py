# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from frappe import _

def get_data():
	return [
		{
			"label": _("Postulante"),
			"icon": "icon-star",
			"items": [
				{
					"type": "doctype",
					"name": "Postulante",
					"description": _("Registro de Postulantes."),
				}
			]
		},
		{
			"label": _("Proceso de Admision"),
			"items": [
				{
					"type": "doctype",
					"name": "Procesos de Admision",
					"description": _("Registro de los procesos de Admision."),
				}

			]
		},
		{
			"label": _("Admision"),
			"icon": "icon-cog",
			"items": [
				{
					"type": "doctype",
					"name": "Examen de Admision",
					"description": _("Registro de Notas del Examen de Admision.")
				},
				{
					"type": "doctype",
					"name": "Entrevista",
					"description": _("Registro de Notas de la Entrevista.")
				},
				{
					"type": "doctype",
					"name": "Evaluacion Psicologica",
					"description": _("Registro de Notas de la Evaluacion Psicologica.")
				},
				{
					"type": "doctype",
					"name": "Resultado Proceso de Admision",
					"description": _("Resultado del Proceso de Admision.")
				}
			]
		}
	]
