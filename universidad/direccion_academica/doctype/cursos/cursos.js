// Copyright (c) 2016, seethersan and contributors
// For license information, please see license.txt
//por implementar
frappe.provide("universidad.cursos");
frappe.ui.form.on('Cursos', {
	refresh: function(frm) {

	}
});


frappe.ui.form.on('Cursos', 'ciclo', function(frm) {
	universidad.cursos.check_mandatory_to_get_prerequisitos(frm);
});


frappe.ui.form.on('Cursos', 'carrera', function(frm) {
	universidad.cursos.check_mandatory_to_get_prerequisitos(frm);
});


universidad.cursos.check_mandatory_to_get_prerequisitos = function(frm) {
	if (frm.doc.ciclo && frm.doc.carrera) {
		universidad.cursos.get_prerequisitos(frm);
	}
}


universidad.cursos.get_prerequisitos = function(frm) {	
	frappe.call({
			method: "get_cursos_requisitos",
			doc: frm.doc,
			callback: function(r) {
				var curso = r.message
				var cursos = []
				for (var i in curso){
					cursos.push(curso[i]['curso'])
				}
				cur_frm.set_query("curso", "cursos_prerequisitos", function() {
		        return {
		        		"filters": [
			                ["nombre_curso", "in", cursos]
		            	]
		        	};
		    	});
			}
		})

}