// Copyright (c) 2016, seethersan and contributors
// For license information, please see license.txt
frappe.provide("universidad.costos_de_carrera");
frappe.ui.form.on('Costos de Carrera', {
	refresh: function(frm) {

	}
});


frappe.ui.form.on('Costos de Carrera', 'carrera', function(frm) {
	universidad.costos_de_carrera.check_mandatory_to_get_codigo(frm);
});


frappe.ui.form.on('Costos de Carrera', 'periodo', function(frm) {
	universidad.costos_de_carrera.check_mandatory_to_get_codigo(frm);
});


universidad.costos_de_carrera.check_mandatory_to_get_codigo = function(frm) {
	if (frm.doc.carrera && frm.doc.periodo) {
		universidad.costos_de_carrera.get_codigo(frm);
	}
}


universidad.costos_de_carrera.get_codigo = function(frm) {
	if (frm.doc.carrera && frm.doc.periodo) {
		frm.set_value("codigo", frm.doc.carrera + "-"+frm.doc.periodo)
	}
}