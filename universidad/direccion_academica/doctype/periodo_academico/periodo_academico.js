// Copyright (c) 2016, seethersan and contributors
// For license information, please see license.txt
frappe.provide("universidad.periodo_academico");
frappe.ui.form.on('Periodo Academico', {
	refresh: function(frm) {

	}
});

frappe.ui.form.on('Periodo Academico', 'fecha_inicio', function(frm) {
	universidad.periodo_academico.check_mandatory_to_get_codigo(frm);
});


frappe.ui.form.on('Periodo Academico', 'fecha_fin', function(frm) {
	universidad.periodo_academico.check_mandatory_to_get_codigo(frm);
});


universidad.periodo_academico.check_mandatory_to_get_codigo = function(frm) {
	if (frm.doc.fecha_inicio && frm.doc.fecha_fin) {
		universidad.periodo_academico.get_codigo(frm);
	}
}


universidad.periodo_academico.get_codigo = function(frm) {
	if (frm.doc.fecha_inicio && frm.doc.fecha_fin) {
		frappe.call({
				method: "set_codigo",
				doc: frm.doc,
				callback: function(r) {
					if(r.message) {
						frm.set_value("codigo_periodo", r.message);
					}
				}
			})
	}
}