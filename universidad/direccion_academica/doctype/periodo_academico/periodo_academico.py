# -*- coding: utf-8 -*-
# Copyright (c) 2015, seethersan and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe.model.document import Document
from datetime import datetime, date

class PeriodoAcademico(Document):
	def validate(self):
		self.validate_date()


	def set_codigo(self):
		if not self.codigo_periodo:
			count = frappe.db.sql("""select (count(name) + 1) as cuenta
					from `tabPeriodo Academico` where YEAR(creation)= %s""",
					date.today().year)
			cuenta = count[0]
			codigo = str(date.today().year) + '-' + str(cuenta[0]).zfill(2)
		else:
			codigo = self.codigo_periodo
		return codigo


	def validate_date(self):
		"""Validates if from_time is greater than to_time"""
		if self.fecha_inicio_matricula > self.fecha_fin_matricula:
			frappe.throw("Fecha Inicio de Matricula no puede ser mayor a Fecha Fin de Matrícula.")
		if self.fecha_inicio_matricula_ext > self.fecha_fin_matricula_ext:
			frappe.throw("Fecha Inicio de Matricula Extemporanea no puede ser mayor a Fecha Fin de Matrícula Extemporanea.")
		if self.fecha_fin_matricula > self.fecha_inicio_matricula_ext:
			frappe.throw("Fecha Fin de Matricula no puede ser mayor a Fecha Inicio de Matrícula Extemporanea.")
		if self.fecha_fin_matricula_ext > self.fecha_inicio:
			frappe.throw("Fecha de Fin Matricula Extemporanea no puede ser mayor a Fecha Inicio de Clases.")
		if self.fecha_inicio > self.fecha_fin:
			frappe.throw("Fecha Inicio de Clases no puede ser mayor a Fecha Fin de Clases.")
		if self.inicio_examen_parcial > self.final_examen_parcial:
			frappe.throw("Fecha de Inicio Examen Parcial no puede ser mayor a Fecha de Fin Examen Parcial.")
		if self.fecha_fin_matricula > self.fecha_inicio:
			frappe.throw("Fecha Fin Matricula no puede ser mayor a Fecha Inicio de Clases.")
		if self.inicio_examen_final > self.fecha_fin:
			frappe.throw("Fecha de Inicio Examen Final no puede ser mayor a Fecha Fin de Clases.")
		if self.inicio_examen_final > self.fin_examen_final:
			frappe.throw("Fecha de Inicio Examen Final no puede ser mayor a Fecha de Fin Examen Final.")
		if self.inicio_examen_parcial > self.fecha_fin:
			frappe.throw("Fecha de Inicio Examen Parcial no puede ser mayor a Fecha Fin de Clases.")
		if self.fecha_inicio > self.inicio_examen_parcial:
			frappe.throw("Fecha Inicio de Clases no puede ser mayor a Fecha de Inicio Examen Parcial.")