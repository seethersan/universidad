// Copyright (c) 2016, seethersan and contributors
// For license information, please see license.txt
frappe.provide("universidad.programacion_de_clases");
frappe.ui.form.on('Programacion de Clases', {	
	refresh: function(frm) {
		frm.disable_save();
		frappe.call({
			method: "get_profesor",
			doc: frm.doc,
			callback: function (r) {
				var profesor = r.message;
				var profesores = [];
				for (var i in profesor){
					profesores.push(profesor[i][0])
				}
				cur_frm.set_query("profesor", function() {
			        return {
		        		"filters": [
			                ["employee_name", "in", profesores]
		            	]
		        	};
		    	});
			}
		})
	}
});


frappe.ui.form.on('Programacion de Clases', 'horario', function(frm) {
	universidad.programacion_de_clases.check_mandatory_to_set_button(frm);
	
	
});


frappe.ui.form.on('Programacion de Clases', 'periodo_academico', function(frm) {
	universidad.programacion_de_clases.check_mandatory_to_set_button(frm);
	
	
});


frappe.ui.form.on('Programacion de Clases', 'curso', function(frm) {
	universidad.programacion_de_clases.check_mandatory_to_set_button(frm);
	
	
});


frappe.ui.form.on('Programacion de Clases', 'profesor', function(frm) {
	universidad.programacion_de_clases.check_mandatory_to_set_button(frm);
	
	
});


universidad.programacion_de_clases.get_capacidad = function(frm) {
	frappe.call({
		method: "get_capacidad",
		doc: frm.doc,
		callback: function (r) {
			if (r.message) {
				var capacidad = r.message[0]
				frm.set_value("capacidad", capacidad["capacidad"])
			}
		}
	})
}


frappe.ui.form.on('Programacion de Clases', 'salon_lunes', function(frm) {
	universidad.programacion_de_clases.get_capacidad(frm);	
});


frappe.ui.form.on('Programacion de Clases', 'salon_martes', function(frm) {
	universidad.programacion_de_clases.get_capacidad(frm);	
});


frappe.ui.form.on('Programacion de Clases', 'salon_miercoles', function(frm) {
	universidad.programacion_de_clases.get_capacidad(frm);	
});


frappe.ui.form.on('Programacion de Clases', 'salon_jueves', function(frm) {
	universidad.programacion_de_clases.get_capacidad(frm);	
});


frappe.ui.form.on('Programacion de Clases', 'salon_viernes', function(frm) {
	universidad.programacion_de_clases.get_capacidad(frm);	
});


frappe.ui.form.on('Programacion de Clases', 'salon_sabado', function(frm) {
	universidad.programacion_de_clases.get_capacidad(frm);	
});


universidad.programacion_de_clases.check_mandatory_to_set_button = function(frm) {
	if (frm.doc.horario && frm.doc.periodo_academico && frm.doc.curso && frm.doc.profesor) {
		frappe.call({
			method: "get_horario",
			doc: frm.doc,
			callback: function (r) {
				if (r.message) {
					horario = r.message
					frm.fields_dict.registro.$input.removeClass("btn-primary");
					frm.fields_dict.eliminar.$input.addClass("btn-primary");
					frm.fields_dict.actualizar.$input.addClass("btn-primary");
					frm.set_value("lunes", horario[0]['lunes']);
					frm.set_value("inicio_lunes", horario[0]['inicio_lunes']);
					frm.set_value("salida_lunes", horario[0]['salida_lunes']);
					frm.set_value("salon_lunes", horario[0]['salon_lunes']);
					frm.set_value("martes", horario[0]['martes']);
					frm.set_value("inicio_martes", horario[0]['inicio_martes']);
					frm.set_value("salida_martes", horario[0]['salida_martes']);
					frm.set_value("salon_martes", horario[0]['salon_martes']);
					frm.set_value("miercoles", horario[0]['miercoles']);
					frm.set_value("inicio_miercoles", horario[0]['inicio_miercoles']);
					frm.set_value("salida_miercoles", horario[0]['salida_miercoles']);
					frm.set_value("salon_miercoles", horario[0]['salon_miercoles']);
					frm.set_value("jueves", horario[0]['jueves']);
					frm.set_value("inicio_jueves", horario[0]['inicio_jueves']);
					frm.set_value("salida_jueves", horario[0]['salida_jueves']);
					frm.set_value("salon_jueves", horario[0]['salon_jueves']);
					frm.set_value("viernes", horario[0]['viernes']);
					frm.set_value("inicio_viernes", horario[0]['inicio_viernes']);
					frm.set_value("salida_viernes", horario[0]['salida_viernes']);
					frm.set_value("salon_viernes", horario[0]['salon_viernes']);
					frm.set_value("sabado", horario[0]['sabado']);
					frm.set_value("inicio_sabado", horario[0]['inicio_sabado']);
					frm.set_value("salida_sabado", horario[0]['salida_sabado']);
					frm.set_value("salon_sabado", horario[0]['salon_sabado']);
					frm.set_value("capacidad", horario[0]['capacidad']);
					frm.set_value("capacidad", horario[0]['cantidad_clases']);
				}
				else {
					frm.fields_dict.registro.$input.addClass("btn-primary");
					frm.fields_dict.eliminar.$input.removeClass("btn-primary");
					frm.fields_dict.actualizar.$input.removeClass("btn-primary");
					frm.set_value("lunes", "");
					frm.set_value("inicio_lunes", "");
					frm.set_value("salida_lunes", "");
					frm.set_value("salon_lunes", "");
					frm.set_value("martes", "");
					frm.set_value("inicio_martes", "");
					frm.set_value("salida_martes", "");
					frm.set_value("salon_martes", "");
					frm.set_value("miercoles", "");
					frm.set_value("inicio_miercoles", "");
					frm.set_value("salida_miercoles", "");
					frm.set_value("salon_miercoles", "");
					frm.set_value("jueves", "");
					frm.set_value("inicio_jueves", "");
					frm.set_value("salida_jueves", "");
					frm.set_value("salon_jueves", "");
					frm.set_value("viernes", "");
					frm.set_value("inicio_viernes", "");
					frm.set_value("salida_viernes", "");
					frm.set_value("salon_viernes", "");
					frm.set_value("sabado", "");
					frm.set_value("inicio_sabado", "");
					frm.set_value("salida_sabado", "");
					frm.set_value("salon_sabado", "");
					frm.set_value("capacidad", "");
					frm.set_value("cantidad_clases", "");				
				}
			}
		})
	}
	else{
		frm.fields_dict.registro.$input.removeClass("btn-primary");
		frm.set_value("lunes", "");
		frm.set_value("inicio_lunes", "");
		frm.set_value("salida_lunes", "");
		frm.set_value("salon_lunes", "");
		frm.set_value("martes", "");
		frm.set_value("inicio_martes", "");
		frm.set_value("salida_martes", "");
		frm.set_value("salon_martes", "");
		frm.set_value("miercoles", "");
		frm.set_value("inicio_miercoles", "");
		frm.set_value("salida_miercoles", "");
		frm.set_value("salon_miercoles", "");
		frm.set_value("jueves", "");
		frm.set_value("inicio_jueves", "");
		frm.set_value("salida_jueves", "");
		frm.set_value("salon_jueves", "");
		frm.set_value("viernes", "");
		frm.set_value("inicio_viernes", "");
		frm.set_value("salida_viernes", "");
		frm.set_value("salon_viernes", "");
		frm.set_value("sabado", "");
		frm.set_value("inicio_sabado", "");
		frm.set_value("salida_sabado", "");
		frm.set_value("salon_sabado", "");
		frm.set_value("capacidad", "");
		frm.set_value("cantidad_clases", "");
	}
}


universidad.programacion_de_clases.check_mandatory_to_fetch = function(frm) {
	$.each(["horario"], function(i, field) {
		if(!frm.doc[frappe.model.scrub(field)]) frappe.throw(__("Please select {0} first", [field]));
	});
	$.each(["periodo_academico"], function(i, field) {
		if(!frm.doc[frappe.model.scrub(field)]) frappe.throw(__("Please select {0} first", [field]));
	});
	$.each(["curso"], function(i, field) {
		if(!frm.doc[frappe.model.scrub(field)]) frappe.throw(__("Please select {0} first", [field]));
	});
	$.each(["profesor"], function(i, field) {
		if(!frm.doc[frappe.model.scrub(field)]) frappe.throw(__("Please select {0} first", [field]));
	});
	var total
	frappe.call({
			method: "get_horario",
			doc: frm.doc,
			callback: function (r) {
				if (r.message) {
					frappe.throw("Horario ya existe")
				}
				else {
					universidad.programacion_de_clases.create_horario_clases(frm);
			}
		}
	})
};


universidad.programacion_de_clases.create_horario_curso = function(frm,total) {
	frappe.call({
		method: "frappe.client.insert",
		args: {
			doc: {
				doctype: "Horario Curso",
				horario: frm.doc.horario,
				curso: frm.doc.curso,
				periodo_academico: frm.doc.periodo_academico,
				profesor: frm.doc.profesor,
				lunes: frm.doc.lunes,
				inicio_lunes: frm.doc.inicio_lunes,
				salida_lunes: frm.doc.salida_lunes,
				salon_lunes: frm.doc.salon_lunes,
				martes: frm.doc.martes,
				inicio_martes: frm.doc.inicio_martes,
				salida_martes: frm.doc.salida_martes,
				salon_martes: frm.doc.salon_martes,
				miercoles: frm.doc.miercoles,
				inicio_miercoles: frm.doc.inicio_miercoles,
				salida_miercoles: frm.doc.salida_miercoles,
				salon_miercoles: frm.doc.salon_miercoles,
				jueves: frm.doc.jueves,
				inicio_jueves: frm.doc.inicio_jueves,
				salida_jueves: frm.doc.salida_jueves,
				salon_jueves: frm.doc.salon_jueves,
				viernes: frm.doc.viernes,
				inicio_viernes: frm.doc.inicio_viernes,
				salida_viernes: frm.doc.salida_viernes,
				salon_viernes: frm.doc.salon_viernes,
				sabado: frm.doc.sabado,
				inicio_sabado: frm.doc.inicio_sabado,
				salida_sabado: frm.doc.salida_sabado,
				salon_sabado: frm.doc.salon_sabado,
				cantidad_clases: total,
				capacidad: frm.doc.capacidad
			}
		}
	});
}


universidad.programacion_de_clases.create_horario_clases = function(frm) {
	frappe.call({
		type: "GET",
		method: "frappe.client.get",
		args: {
			doctype: "Periodo Academico",
			name: frm.doc.periodo_academico
		},
		callback: function(data) {
			var inicio = data.message.fecha_inicio.split('-')
			var fecha_inicio = new Date(inicio[0],inicio[1]-1,inicio[2])
			var fin = data.message.fecha_fin.split('-')
			var fecha_fin = new Date(fin[0],fin[1]-1,fin[2])
			var inicio_parcial = data.message.inicio_examen_parcial.split('-')
			var fecha_inicio_parcial = new Date(inicio_parcial[0],inicio_parcial[1]-1,inicio_parcial[2])
			var final_parcial = data.message.final_examen_parcial.split('-')
			var fecha_final_parcial = new Date(final_parcial[0],final_parcial[1]-1,final_parcial[2])
			var inicio_final = data.message.inicio_examen_final.split('-')
			var fecha_inicio_final = new Date(inicio_final[0],inicio_final[1]-1,inicio_final[2])
			var fin_final = data.message.fin_examen_final.split('-')
			var fecha_fin_final = new Date(fin_final[0],fin_final[1]-1,fin_final[2])
			var total = 0
			if (frm.doc.lunes) {
				var fecha_lunes = new Date(fecha_inicio)
				while (fecha_lunes.getDay()!=1) {
					fecha_lunes.setDate(fecha_lunes.getDate() + 1)
				}						
				while (+fecha_lunes <= +fecha_fin) {
					if (+fecha_lunes >= +fecha_inicio_parcial && +fecha_lunes <= +fecha_final_parcial || +fecha_lunes >= +fecha_inicio_final && +fecha_lunes <= +fecha_fin_final) {

					}
					else {
						frappe.call({
							method: "frappe.client.insert",
							args: {
								doc: {
									doctype: "Horario de Clases",
									horario: frm.doc.horario,
									curso: frm.doc.curso,
									periodo_academico: frm.doc.periodo_academico,
									profesor: frm.doc.profesor,
									fecha: fecha_lunes,
									hora_inicio: frm.doc.inicio_lunes,
									hora_salida: frm.doc.salida_lunes,
									salon: frm.doc.salon_lunes,
									reprogramado: 0
								}
							}
						});
					}
					fecha_lunes.setDate(fecha_lunes.getDate() + 7)	
					total = total + 1							
				}
			}
			if (frm.doc.martes) {
				var fecha_martes = new Date(fecha_inicio)
				while (fecha_martes.getDay()!=2) {
					fecha_martes.setDate(fecha_martes.getDate() + 1)
				}						
				while (+fecha_martes <= +fecha_fin) {
					if (+fecha_martes >= +fecha_inicio_parcial && +fecha_martes <= +fecha_final_parcial || +fecha_martes >= +fecha_inicio_final && +fecha_martes <= +fecha_fin_final) {

					}
					else {
						frappe.call({
							method: "frappe.client.insert",
							args: {
								doc: {
									doctype: "Horario de Clases",
									horario: frm.doc.horario,
									curso: frm.doc.curso,
									periodo_academico: frm.doc.periodo_academico,
									profesor: frm.doc.profesor,
									fecha: fecha_martes,
									hora_inicio: frm.doc.inicio_martes,
									hora_salida: frm.doc.salida_martes,
									salon: frm.doc.salon_martes,
									reprogramado: 0
								}
							}
						});
					}
					fecha_martes.setDate(fecha_martes.getDate() + 7)
					total = total + 1
				}
			}
			if (frm.doc.miercoles) {
				var fecha_miercoles = new Date(fecha_inicio)
				while (fecha_miercoles.getDay()!=3) {
					fecha_miercoles.setDate(fecha_miercoles.getDate() + 1)
				}						
				while (+fecha_miercoles <= +fecha_fin) {
					if (+fecha_miercoles >= +fecha_inicio_parcial && +fecha_miercoles <= +fecha_final_parcial || +fecha_miercoles >= +fecha_inicio_final && +fecha_miercoles <= +fecha_fin_final) {

					}
					else {
						frappe.call({
							method: "frappe.client.insert",
							args: {
								doc: {
									doctype: "Horario de Clases",
									horario: frm.doc.horario,
									curso: frm.doc.curso,
									periodo_academico: frm.doc.periodo_academico,
									profesor: frm.doc.profesor,
									fecha: fecha_miercoles,
									hora_inicio: frm.doc.inicio_miercoles,
									hora_salida: frm.doc.salida_miercoles,
									salon: frm.doc.salon_miercoles,
									reprogramado: 0
								}
							}
						});
					}
					fecha_miercoles.setDate(fecha_miercoles.getDate() + 7)
					total = total + 1
				}
			}
			if (frm.doc.jueves) {
				var fecha_jueves = new Date(fecha_inicio)
				console.log(fecha_jueves)
				console.log(fecha_inicio)
				while (fecha_jueves.getDay()!=4) {
					fecha_jueves.setDate(fecha_jueves.getDate() + 1)
					console.log(fecha_jueves)
				}						
				while (+fecha_jueves <= +fecha_fin) {
					console.log(fecha_jueves)
					if (+fecha_jueves >= +fecha_inicio_parcial && +fecha_jueves <= +fecha_final_parcial || +fecha_jueves >= +fecha_inicio_final && +fecha_jueves <= +fecha_fin_final) {

					}
					else {
						frappe.call({
							method: "frappe.client.insert",
							args: {
								doc: {
									doctype: "Horario de Clases",
									horario: frm.doc.horario,
									curso: frm.doc.curso,
									periodo_academico: frm.doc.periodo_academico,
									profesor: frm.doc.profesor,
									fecha: fecha_jueves,
									hora_inicio: frm.doc.inicio_jueves,
									hora_salida: frm.doc.salida_jueves,
									salon: frm.doc.salon_jueves,
									reprogramado: 0
								}
							}
						});
					}
					fecha_jueves.setDate(fecha_jueves.getDate() + 7)
					total = total + 1
				}
			}
			if (frm.doc.viernes) {
				var fecha_viernes = new Date(fecha_inicio)
				while (fecha_viernes.getDay()!=5) {
					fecha_viernes.setDate(fecha_viernes.getDate() + 1)
				}						
				while (+fecha_viernes <= +fecha_fin) {
					if (+fecha_viernes >= +fecha_inicio_parcial && +fecha_viernes <= +fecha_final_parcial || +fecha_viernes >= +fecha_inicio_final && +fecha_viernes <= +fecha_fin_final) {

					}
					else {
						frappe.call({
							method: "frappe.client.insert",
							args: {
								doc: {
									doctype: "Horario de Clases",
									horario: frm.doc.horario,
									curso: frm.doc.curso,
									periodo_academico: frm.doc.periodo_academico,
									profesor: frm.doc.profesor,
									fecha: fecha_viernes,
									hora_inicio: frm.doc.inicio_viernes,
									hora_salida: frm.doc.salida_viernes,
									salon: frm.doc.salon_viernes,
									reprogramado: 0
								}
							}
						});
					}
					fecha_viernes.setDate(fecha_viernes.getDate() + 7)
					total = total + 1
				}
			}
			if (frm.doc.sabado) {
				var fecha_sabado = new Date(fecha_inicio)
				while (fecha_sabado.getDay()!=6) {
					fecha_sabado.setDate(fecha_sabado.getDate() + 1)
				}						
				while (+fecha_sabado <= +fecha_fin) {
					if (+fecha_sabado >= +fecha_inicio_parcial && +fecha_sabado <= +fecha_final_parcial || +fecha_sabado >= +fecha_inicio_final && +fecha_sabado <= +fecha_fin_final) {

					}
					else {
						frappe.call({
							method: "frappe.client.insert",
							args: {
								doc: {
									doctype: "Horario de Clases",
									horario: frm.doc.horario,
									curso: frm.doc.curso,
									periodo_academico: frm.doc.periodo_academico,
									profesor: frm.doc.profesor,
									fecha: fecha_sabado,
									hora_inicio: frm.doc.inicio_sabado,
									hora_salida: frm.doc.salida_sabado,
									salon: frm.doc.salon_sabado,
									reprogramado: 0
								}
							}
						});
					}
					fecha_sabado.setDate(fecha_sabado.getDate() + 7)
					total = total + 1
				}
			}
			frm.set_value("cantidad_clases", total);
			universidad.programacion_de_clases.create_horario_curso(frm, total);
		}
	});
	frappe.throw("Horario creado con exito");
};


frappe.ui.form.on('Programacion de Clases', 'registro', function(frm) {
	frappe.call({
		method: "get_horario",
		doc: frm.doc,
		callback: function (r) {
			if (r.message) {
				frappe.throw("Horario ya existe")
			}
			else {
				frappe.call({
					method: "validate_horario",
					doc: frm.doc,
					callback: function (r) {
						if (r.message) {
							universidad.programacion_de_clases.check_mandatory_to_fetch(frm);
							frm.fields_dict.registro.$input.removeClass("btn-primary");
							frm.fields_dict.eliminar.$input.addClass("btn-primary");
							frm.fields_dict.actualizar.$input.addClass("btn-primary");
						}
					}
				});
			}
		}
	})	
		
});


frappe.ui.form.on('Programacion de Clases', 'eliminar', function(frm) {
	frappe.call({
		method: "get_horario",
		doc: frm.doc,
		callback: function (r) {
			if (r.message) {
				frappe.call({
					method: "delete_horario",
					doc: frm.doc,
					callback: function (r) {
						frm.fields_dict.registro.$input.addClass("btn-primary");
						frm.fields_dict.eliminar.$input.removeClass("btn-primary");
						frm.fields_dict.actualizar.$input.removeClass("btn-primary");
						frappe.throw("Horario eliminado");
					}
				});				
			}
			else {
				frappe.throw("Horario no existe")
			}
		}
	})		
});


frappe.ui.form.on('Programacion de Clases', 'actualizar', function(frm) {
	frappe.call({
		method: "get_horario",
		doc: frm.doc,
		callback: function (r) {
			if (r.message) {
				frappe.call({
					method: "update_horario",
					doc: frm.doc,
					callback: function (r) {
						universidad.programacion_de_clases.check_mandatory_to_fetch(frm);
						frm.fields_dict.registro.$input.removeClass("btn-primary");
						frm.fields_dict.eliminar.$input.addClass("btn-primary");
						frm.fields_dict.actualizar.$input.addClass("btn-primary");
					}
				});
			}
			else {
				frappe.throw("Horario no existe")
			}
		}
	});
});




