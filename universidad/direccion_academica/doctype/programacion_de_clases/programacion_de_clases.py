# -*- coding: utf-8 -*-
# Copyright (c) 2015, seethersan and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe.model.document import Document
from universidad.universidad.utils import Utils

class ProgramaciondeClases(Utils):
	def validate_horario(self):
		save = True
		save = self.validate_overlap()
		save = self.validate_date()
		self.validate_day()
		return save


	def get_capacidad(self):
		salon = []
		capacidad = []
		if self.salon_lunes:
			if not self.salon_lunes in salon:
				salon.append(self.salon_lunes)
		if self.salon_martes:
			if not self.salon_martes in salon:
				salon.append(self.salon_martes)
		if self.salon_miercoles:
			if not self.salon_miercoles in salon:
				salon.append(self.salon_miercoles)
		if self.salon_jueves:
			if not self.salon_jueves in salon:
				salon.append(self.salon_jueves)
		if self.salon_viernes:
			if not self.salon_viernes in salon:
				salon.append(self.salon_viernes)
		if self.salon_sabado:
			if not self.salon_sabado in salon:
				salon.append(self.salon_sabado)
		lista_salon = "('"+"', '".join(str(x) for x in salon) + "')"
		if salon:
			capacidad = frappe.db.sql("""select min(capacidad) as capacidad
					from `tabSalon` where nombre in """ + lista_salon, as_dict=1)
		return capacidad


	def get_horario(self):
		horarios = frappe.db.sql("""select 
										lunes as lunes,
										inicio_lunes as inicio_lunes,
										salida_lunes as salida_lunes,
										salon_lunes as salon_lunes,
										martes as martes,
										inicio_martes as inicio_martes,
										salida_martes as salida_martes,
										salon_martes as salon_martes,
										miercoles as miercoles,
										inicio_miercoles as inicio_miercoles,
										salida_miercoles as salida_miercoles,
										salon_miercoles as salon_miercoles,
										jueves as jueves,
										inicio_jueves as inicio_jueves,
										salida_jueves as salida_jueves,
										salon_jueves as salon_jueves,
										viernes as viernes,
										inicio_viernes as inicio_viernes,
										salida_viernes as salida_viernes,
										salon_viernes as salon_viernes,
										sabado as sabado,
										inicio_sabado as inicio_sabado,
										salida_sabado as salida_sabado,
										salon_sabado as salon_sabado
				from `tabHorario Curso` where horario= %s
				and curso=%s
				and periodo_academico=%s
				and profesor=%s""", 
				(self.horario, self.curso, self.periodo_academico, self.profesor), as_dict=1)
		return horarios


	def delete_horario(self):
		frappe.db.sql("""delete from `tabHorario Curso` where horario=%s
			and curso=%s
			and periodo_academico=%s
			and profesor=%s""",
		(self.horario, self.curso, self.periodo_academico, self.profesor))
		frappe.db.sql("""delete from `tabHorario de Clases` where horario=%s
			and curso=%s
			and periodo_academico=%s
			and profesor=%s""",
		(self.horario, self.curso, self.periodo_academico, self.profesor))


	def update_horario(self):
		self.delete_horario()
		self.validate_horario()
		

	def validate_date(self):
		"""Validates if from_time is greater than to_time"""
		save = True
		if self.lunes:
			if self.inicio_lunes > self.salida_lunes:
				frappe.throw("From Time cannot be greater than To Time.")
				save = false
		if self.martes:
			if self.inicio_martes > self.salida_martes:
				frappe.throw("From Time cannot be greater than To Time.")
				save = false
		if self.miercoles:
			if self.inicio_miercoles > self.salida_miercoles:
				frappe.throw("From Time cannot be greater than To Time.")
				save = false
		if self.jueves:
			if self.inicio_jueves > self.salida_jueves:
				frappe.throw("From Time cannot be greater than To Time.")
				save = false
		if self.viernes:
			if self.inicio_viernes > self.salida_viernes:
				frappe.throw("From Time cannot be greater than To Time.")
				save = false
		if self.sabado:
			if self.inicio_sabado > self.salida_sabado:
				frappe.throw("From Time cannot be greater than To Time.")
				save = false
		return save


	def validate_day(self):
		"""Validates if from_time is greater than to_time"""
		if not self.lunes:
			self.inicio_lunes = ""
			self.salida_lunes = ""
			self.salon_lunes = ""
		if not self.martes:
			self.inicio_martes = ""
			self.salida_martes = ""
			self.salon_martes = ""
		if not self.miercoles:
			self.inicio_miercoles = ""
			self.salida_miercoles = ""
			self.salon_miercoles = ""
		if not self.jueves:
			self.inicio_jueves = ""
			self.salida_jueves = ""
			self.salon_jueves = ""
		if not self.viernes:
			self.inicio_viernes = ""
			self.salida_viernes = ""
			self.salon_viernes = ""
		if not self.sabado:
			self.inicio_sabado = ""
			self.salida_sabado = ""
			self.salon_sabado = ""
	

	def validate_overlap(self):
		"""Validates overlap for Student Group, Instructor, Room"""
		
		from universidad.universidad.utils import validate_overlap_for

		save = True

		if self.lunes:
			save = validate_overlap_for(self, "Horario Curso", "profesor", self.profesor, "lunes", self.inicio_lunes, self.salida_lunes, self.periodo_academico)
			save = validate_overlap_for(self, "Horario Curso", "salon_lunes", self.salon_lunes, "lunes", self.inicio_lunes, self.salida_lunes, self.periodo_academico)
			save = validate_overlap_for(self, "Horario Curso", "horario", self.horario, "lunes", self.inicio_lunes, self.salida_lunes, self.periodo_academico)
		if self.martes:
			save = validate_overlap_for(self, "Horario Curso", "profesor", self.profesor, "martes", self.inicio_martes, self.salida_martes, self.periodo_academico)
			save = validate_overlap_for(self, "Horario Curso", "salon_martes", self.salon_martes, "martes", self.inicio_martes, self.salida_martes, self.periodo_academico)
			save = validate_overlap_for(self, "Horario Curso", "horario", self.horario, "martes", self.inicio_martes, self.salida_martes, self.periodo_academico)
		if self.miercoles:
			save = validate_overlap_for(self, "Horario Curso", "profesor", self.profesor, "miercoles", self.inicio_miercoles, self.salida_miercoles, self.periodo_academico)
			save = validate_overlap_for(self, "Horario Curso", "salon_miercoles", self.salon_miercoles, "miercoles", self.inicio_miercoles, self.salida_miercoles, self.periodo_academico)
			save = validate_overlap_for(self, "Horario Curso", "horario", self.horario, "miercoles", self.inicio_miercoles, self.salida_miercoles, self.periodo_academico)
		if self.jueves:
			save = validate_overlap_for(self, "Horario Curso", "profesor", self.profesor, "jueves", self.inicio_jueves, self.salida_jueves, self.periodo_academico)
			save = validate_overlap_for(self, "Horario Curso", "salon_jueves", self.salon_jueves, "jueves", self.inicio_jueves, self.salida_jueves, self.periodo_academico)
			save = validate_overlap_for(self, "Horario Curso", "horario", self.horario, "jueves", self.inicio_jueves, self.salida_jueves, self.periodo_academico)
		if self.viernes:
			save = validate_overlap_for(self, "Horario Curso", "profesor", self.profesor, "viernes", self.inicio_viernes, self.salida_viernes, self.periodo_academico)
			save = validate_overlap_for(self, "Horario Curso", "salon_viernes", self.salon_viernes, "viernes", self.inicio_viernes, self.salida_viernes, self.periodo_academico)
			save = validate_overlap_for(self, "Horario Curso", "horario", self.horario, "viernes", self.inicio_viernes, self.salida_viernes, self.periodo_academico)
		if self.sabado:
			save = validate_overlap_for(self, "Horario Curso", "profesor", self.profesor, "sabado", self.inicio_sabado, self.salida_sabado, self.periodo_academico)
			save = validate_overlap_for(self, "Horario Curso", "salon_sabado", self.salon_sabado, "sabado", self.inicio_sabado, self.salida_sabado, self.periodo_academico)
			save = validate_overlap_for(self, "Horario Curso", "horario", self.horario, "sabado", self.inicio_sabado, self.salida_sabado, self.periodo_academico)
		return save
