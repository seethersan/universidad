// Copyright (c) 2016, seethersan and contributors
// For license information, please see license.txt
frappe.provide("universidad.horario_curso");
frappe.ui.form.on('Horario Curso', {
	refresh: function(frm) {
		frappe.call({
			method: "get_profesor",
			doc: frm.doc,
			callback: function (r) {
				var profesor = r.message;
				var profesores = [];
				for (var i in profesor){
					profesores.push(profesor[i][0])
				}
				cur_frm.set_query("profesor", function() {
			        return {
		        		"filters": [
			                ["employee_name", "in", profesores]
		            	]
		        	};
		    	});
			}
		})
	}
});



