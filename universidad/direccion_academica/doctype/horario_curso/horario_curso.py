# -*- coding: utf-8 -*-
# Copyright (c) 2015, seethersan and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe.model.document import Document
from universidad.universidad.utils import Utils

class HorarioCurso(Utils):
	def validate_horario(self):
		self.validate_overlap()
		self.validate_date()
		self.validate_day()


	def validate_date(self):
		"""Validates if from_time is greater than to_time"""
		if self.lunes:
			if self.inicio_lunes > self.salida_lunes:
				frappe.throw("From Time cannot be greater than To Time.")
		if self.martes:
			if self.inicio_martes > self.salida_martes:
				frappe.throw("From Time cannot be greater than To Time.")
		if self.miercoles:
			if self.inicio_miercoles > self.salida_miercoles:
				frappe.throw("From Time cannot be greater than To Time.")
		if self.jueves:
			if self.inicio_jueves > self.salida_jueves:
				frappe.throw("From Time cannot be greater than To Time.")
		if self.viernes:
			if self.inicio_viernes > self.salida_viernes:
				frappe.throw("From Time cannot be greater than To Time.")
		if self.sabado:
			if self.inicio_sabado > self.salida_sabado:
				frappe.throw("From Time cannot be greater than To Time.")


	def validate_day(self):
		"""Validates if from_time is greater than to_time"""
		if not self.lunes:
			self.inicio_lunes = ""
			self.salida_lunes = ""
			self.salon_lunes = ""
		if not self.martes:
			self.inicio_martes = ""
			self.salida_martes = ""
			self.salon_martes = ""
		if not self.miercoles:
			self.inicio_miercoles = ""
			self.salida_miercoles = ""
			self.salon_miercoles = ""
		if not self.jueves:
			self.inicio_jueves = ""
			self.salida_jueves = ""
			self.salon_jueves = ""
		if not self.viernes:
			self.inicio_viernes = ""
			self.salida_viernes = ""
			self.salon_viernes = ""
		if not self.sabado:
			self.inicio_sabado = ""
			self.salida_sabado = ""
			self.salon_sabado = ""
	

	def validate_overlap(self):
		"""Validates overlap for Student Group, Instructor, Room"""
		
		from universidad.universidad.utils import validate_overlap_for

		if self.lunes:
			validate_overlap_for(self, "Horario Curso", "profesor", self.profesor, "lunes", self.inicio_lunes, self.salida_lunes)
			validate_overlap_for(self, "Horario Curso", "salon_lunes", self.salon_lunes, "lunes", self.inicio_lunes, self.salida_lunes)
			validate_overlap_for(self, "Horario Curso", "horario", self.horario, "lunes", self.inicio_lunes, self.salida_lunes)
		if self.martes:
			validate_overlap_for(self, "Horario Curso", "profesor", self.profesor, "martes", self.inicio_martes, self.salida_martes)
			validate_overlap_for(self, "Horario Curso", "salon_martes", self.salon_martes, "martes", self.inicio_martes, self.salida_martes)
			validate_overlap_for(self, "Horario Curso", "horario", self.horario, "martes", self.inicio_martes, self.salida_martes)
		if self.miercoles:
			validate_overlap_for(self, "Horario Curso", "profesor", self.profesor, "miercoles", self.inicio_miercoles, self.salida_miercoles)
			validate_overlap_for(self, "Horario Curso", "salon_miercoles", self.salon_miercoles, "miercoles", self.inicio_miercoles, self.salida_miercoles)
			validate_overlap_for(self, "Horario Curso", "horario", self.horario, "miercoles", self.inicio_miercoles, self.salida_miercoles)
		if self.jueves:
			validate_overlap_for(self, "Horario Curso", "profesor", self.profesor, "jueves", self.inicio_jueves, self.salida_jueves)
			validate_overlap_for(self, "Horario Curso", "salon_jueves", self.salon_jueves, "jueves", self.inicio_jueves, self.salida_jueves)
			validate_overlap_for(self, "Horario Curso", "horario", self.horario, "jueves", self.inicio_jueves, self.salida_jueves)
		if self.viernes:
			validate_overlap_for(self, "Horario Curso", "profesor", self.profesor, "viernes", self.inicio_viernes, self.salida_viernes)
			validate_overlap_for(self, "Horario Curso", "salon_viernes", self.salon_viernes, "viernes", self.inicio_viernes, self.salida_viernes)
			validate_overlap_for(self, "Horario Curso", "horario", self.horario, "viernes", self.inicio_viernes, self.salida_viernes)
		if self.sabado:
			validate_overlap_for(self, "Horario Curso", "profesor", self.profesor, "sabado", self.inicio_sabado, self.salida_sabado)
			validate_overlap_for(self, "Horario Curso", "salon_sabado", self.salon_sabado, "sabado", self.inicio_sabado, self.salida_sabado)
			validate_overlap_for(self, "Horario Curso", "horario", self.horario, "sabado", self.inicio_sabado, self.salida_sabado)
