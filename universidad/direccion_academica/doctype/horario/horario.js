// Copyright (c) 2016, seethersan and contributors
// For license information, please see license.txt
frappe.provide("universidad.horario");
frappe.ui.form.on('Horario', {
	refresh: function(frm) {

	}
});


frappe.ui.form.on('Horario', 'carrera', function(frm) {
	universidad.horario.check_mandatory_to_set_codigo(frm);
});


frappe.ui.form.on('Horario', 'ciclo', function(frm) {
	universidad.horario.check_mandatory_to_set_codigo(frm);
});


frappe.ui.form.on('Horario', 'turno', function(frm) {
	universidad.horario.check_mandatory_to_set_codigo(frm);
});


universidad.horario.check_mandatory_to_set_codigo = function(frm) {
	if (frm.doc.carrera && frm.doc.ciclo && frm.doc.turno) {
		universidad.horario.set_codigo(frm);
	}
}


universidad.horario.set_codigo = function(frm) {
	if (frm.doc.carrera && frm.doc.ciclo && frm.doc.turno) {
		frm.set_value("nombre", frm.doc.carrera.charAt(0) + frm.doc.ciclo + frm.doc.turno.charAt(0))
	}
}