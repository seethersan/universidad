// Copyright (c) 2016, seethersan and contributors
// For license information, please see license.txt
frappe.provide("universidad.reprogramacion_de_clases");
frappe.ui.form.on('Reprogramacion de Clases', {
	refresh: function(frm) {

	}
});


frappe.ui.form.on('Reprogramacion de Clases', 'curso', function(frm, cdt, cdn) {
	universidad.reprogramacion_de_clases.check_mandatory_to_set_button(frm);
	universidad.reprogramacion_de_clases.check_mandatory_to_set_fecha(frm);
	universidad.reprogramacion_de_clases.check_mandatory_to_get_horario_reprogramado(frm, cdt, cdn);
	frappe.model.set_value(cdt, cdn, "horario", "");
	frappe.model.set_value(cdt, cdn, "salon", "");
});


frappe.ui.form.on('Reprogramacion de Clases', 'profesor', function(frm, cdt, cdn) {
	universidad.reprogramacion_de_clases.check_mandatory_to_set_button(frm);
	universidad.reprogramacion_de_clases.check_mandatory_to_set_fecha(frm);
	universidad.reprogramacion_de_clases.check_mandatory_to_get_horario_reprogramado(frm, cdt, cdn);
	frappe.model.set_value(cdt, cdn, "horario", "");
	frappe.model.set_value(cdt, cdn, "salon", "");
});


frappe.ui.form.on('Reprogramacion de Clases', 'hora_posponer_inicio', function(frm) {
	universidad.reprogramacion_de_clases.check_mandatory_to_set_button(frm);
	
});


frappe.ui.form.on('Reprogramacion de Clases', 'hora_posponer_fin', function(frm) {
	universidad.reprogramacion_de_clases.check_mandatory_to_set_button(frm);
	
});


frappe.ui.form.on('Reprogramacion de Clases', 'salon_posponer', function(frm) {
	universidad.reprogramacion_de_clases.check_mandatory_to_set_button(frm);
	
});


frappe.ui.form.on('Reprogramacion de Clases', 'fecha_posponer', function(frm) {
	universidad.reprogramacion_de_clases.check_mandatory_to_set_button(frm);
	
});


frappe.ui.form.on('Reprogramacion de Clases', 'fecha', function(frm, cdt, cdn) {
	universidad.reprogramacion_de_clases.check_mandatory_to_get_horario_reprogramado(frm, cdt, cdn);
	if (frm.doc.fecha) {
		frappe.call({
			method: "get_datos_horario",
			doc: frm.doc,
			callback: function (r){
				if (r.message) {
					var datos = r.message;
					frappe.model.set_value(cdt, cdn, "horario", datos[0]['horario']);
					frappe.model.set_value(cdt, cdn, "salon", datos[0]['salon']);
				}
			}
		})
	}
	else {
		frappe.model.set_value(cdt, cdn, "horario", "");
		frappe.model.set_value(cdt, cdn, "salon", "");
	}
});


universidad.reprogramacion_de_clases.check_mandatory_to_set_button = function(frm) {
	if (frm.doc.curso && frm.doc.profesor && frm.doc.fecha_posponer && frm.doc.hora_posponer_fin && frm.doc.hora_posponer_inicio && frm.doc.salon_posponer) {
		frm.fields_dict.reprogramar.$input.addClass("btn-primary");

	}
	else{
		frm.fields_dict.reprogramar.$input.removeClass("btn-primary");
		
	}
}


universidad.reprogramacion_de_clases.check_mandatory_to_get_horario_reprogramado = function(frm,cdt,cdn) {
	if (frm.doc.curso && frm.doc.profesor && frm.doc.fecha) {
		frappe.call({
			method: "get_horario_reprogramado",
			doc: frm.doc,
			callback: function (r) {
				if (r.message) {
					var horario = r.message;
					frappe.model.set_value(cdt, cdn, "fecha_posponer", horario[0]['fecha'])
					frappe.model.set_value(cdt, cdn, "hora_posponer_inicio", horario[0]['hora_inicio'])
					frappe.model.set_value(cdt, cdn, "hora_posponer_fin", horario[0]['hora_fin'])
					frappe.model.set_value(cdt, cdn, "salon_posponer", horario[0]['salon'])
					frm.fields_dict.reprogramar.$input.removeClass("btn-primary");
					frm.fields_dict.actualizar.$input.addClass("btn-primary");
					frm.fields_dict.eliminar.$input.addClass("btn-primary");
				}
				else {
					frm.fields_dict.reprogramar.$input.addClass("btn-primary");
					frm.fields_dict.actualizar.$input.removeClass("btn-primary");
					frm.fields_dict.eliminar.$input.removeClass("btn-primary");
				}
			}
		})		
	}
	else{
		frm.fields_dict.reprogramar.$input.removeClass("btn-primary");
		frm.fields_dict.actualizar.$input.removeClass("btn-primary");
		frm.fields_dict.eliminar.$input.removeClass("btn-primary");
	}
}


universidad.reprogramacion_de_clases.check_mandatory_to_set_fecha = function(frm) {
	if (frm.doc.curso && frm.doc.profesor) {
		var fecha = document.querySelectorAll("[data-fieldname='fecha']")[1];
		fecha.options.length = 0;
		var option = document.createElement("option");
		option.text = "";
		fecha.add(option);
		frappe.call({
			method: "get_horario_reprogramacion",
			doc: frm.doc,
			callback: function (r){
				if (r.message) {
					var horarios = r.message;
					for (var i in horarios) {
						var option = document.createElement("option");
						option.text = horarios[i]['fecha'];
						fecha.add(option);
					}
				}
			}
		})
	}
}


universidad.reprogramacion_de_clases.check_mandatory_to_fetch = function(frm) {	
	if (frm.doc.fecha_posponer && frm.doc.hora_posponer_inicio && frm.doc.hora_posponer_fin && frm.doc.salon_posponer) {
		frappe.call({
			method: "set_horario_reprogramado",
			doc: frm.doc
		})
		frappe.throw("Horario reprogramado");
	}	
	else {
		$.each(["fecha_posponer"], function(i, field) {
			if(!frm.doc[frappe.model.scrub(field)]) frappe.throw(__("Please select {0} first", [field]));
		});
		$.each(["hora_posponer_inicio"], function(i, field) {
			if(!frm.doc[frappe.model.scrub(field)]) frappe.throw(__("Please select {0} first", [field]));
		});
		$.each(["hora_posponer_fin"], function(i, field) {
			if(!frm.doc[frappe.model.scrub(field)]) frappe.throw(__("Please select {0} first", [field]));
		});
		$.each(["salon_posponer"], function(i, field) {
			if(!frm.doc[frappe.model.scrub(field)]) frappe.throw(__("Please select {0} first", [field]));
		});
	}
}


frappe.ui.form.on('Reprogramacion de Clases', 'reprogramar', function(frm) {
	frappe.call({
		method: "get_horario_reprogramado",
		doc: frm.doc,
		callback: function (r) {
			if (r.message){
				frappe.throw("Horario Reprogramado ya registrado");
			}
			else {
				frappe.call({
					method: "validate_horario",
					doc: frm.doc,
					callback: function (r) {
						if (r.message) {
							frm.fields_dict.reprogramar.$input.removeClass("btn-primary");
							frm.fields_dict.actualizar.$input.addClass("btn-primary");
							frm.fields_dict.eliminar.$input.addClass("btn-primary");
							universidad.reprogramacion_de_clases.check_mandatory_to_fetch(frm);							
						}
						else {
							frappe.throw("Horario se cruza");
						}  
					}
				})
			}
		}
	})	
});


frappe.ui.form.on('Reprogramacion de Clases', 'eliminar', function(frm) {
	frappe.call({
		method: "get_horario_reprogramado",
		doc: frm.doc,
		callback: function (r) {
			if (r.message){
				frappe.call({
					method: "eliminar_horario_reprogramado",
					doc: frm.doc,
					callback: function (r) {
						frm.fields_dict.reprogramar.$input.addClass("btn-primary");
						frm.fields_dict.eliminar.$input.removeClass("btn-primary");
						frm.fields_dict.actualizar.$input.removeClass("btn-primary");
						frappe.throw("Horario eliminado");
					}
				})
			}
			else {
				frappe.throw("Horario Reprogramado no existe");
			}
		}
	})	
});


frappe.ui.form.on('Reprogramacion de Clases', 'actualizar', function(frm) {
	frappe.call({
		method: "get_horario_reprogramado",
		doc: frm.doc,
		callback: function (r) {
			if (r.message){
				frappe.call({
					method: "set_horario_reprogramado",
					doc: frm.doc,
					callback: function (r) {
						frm.fields_dict.reprogramar.$input.removeClass("btn-primary");
						frm.fields_dict.eliminar.$input.addClass("btn-primary");
						frm.fields_dict.actualizar.$input.addClass("btn-primary");
						frappe.throw("Horario actualizado");
					}
				})
			}
			else {
				frappe.throw("Horario no registrado");
			}
		}
	})	
});

