# -*- coding: utf-8 -*-
# Copyright (c) 2015, seethersan and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
import datetime
from frappe.model.document import Document
from universidad.universidad.utils import Utils

class ReprogramaciondeClases(Utils):
	def validate_horario(self):
		save = True
		save = self.validate_overlap()
		return save


	def get_datos_horario(self):
		datos = frappe.db.sql("""select CONCAT(hora_inicio,'-',hora_salida) as horario, salon as salon
			from `tabHorario de Clases` where curso = %(curso)s
			and profesor = %(profesor)s
			and periodo_academico = %(periodo)s
			and fecha = %(fecha)s""",
			{
				"curso": self.curso,
				"profesor": self.profesor,
				"periodo": self.get_periodo()['periodo'],
				"fecha": self.fecha
			}, as_dict=1)
		return datos


	def get_horario_reprogramado(self):
		horario = frappe.db.sql("""select fecha_reprogramacion as fecha, 
										hora_inicio_reprogramacion as hora_inicio,
										hora_fin_reprogramacion as hora_fin,
										salon_reprogramacion as salon
			from `tabHorario de Clases` 
			where curso = %(curso)s
			and profesor = %(profesor)s
			and periodo_academico = %(periodo)s
			and fecha = %(fecha)s
			and reprogramado = true""",
			{
				"curso": self.curso,
				"profesor": self.profesor,
				"periodo": self.get_periodo()['periodo'],
				"fecha": self.fecha
			}, as_dict=1)
		return horario


	def set_horario_reprogramado(self):
		frappe.db.sql("""update `tabHorario de Clases`
						set fecha_reprogramacion = %(fecha_reprogramacion)s,
							hora_inicio_reprogramacion = %(inicio)s,
							hora_fin_reprogramacion = %(fin)s,
							salon_reprogramacion = %(salon)s,
							reprogramado = true
			where curso = %(curso)s
			and profesor = %(profesor)s
			and periodo_academico = %(periodo)s
			and fecha = %(fecha)s
			and reprogramado = false""",
			{
				"fecha_reprogramacion": self.fecha_posponer,
				"inicio": self.hora_posponer_inicio,
				"fin": self.hora_posponer_fin,
				"salon": self.salon_posponer,
				"curso": self.curso,
				"profesor": self.profesor,
				"periodo": self.get_periodo()['periodo'],
				"fecha": self.fecha
			}, as_dict=1)


	def eliminar_horario_reprogramado(self):
		frappe.db.sql("""update `tabHorario de Clases`
						 set reprogramado = false
			where curso = %(curso)s
			and profesor = %(profesor)s
			and periodo_academico = %(periodo)s
			and fecha = %(fecha)s""",
			{
				"curso": self.curso,
				"profesor": self.profesor,
				"periodo": self.get_periodo()['periodo'],
				"fecha": self.fecha
			}, as_dict=1)


	def validate_overlap(self):
		fecha = self.fecha_posponer.split("-")
		dia = datetime.date(int(fecha[0]),int(fecha[1]),int(fecha[2])).weekday()
		salon = "salon_"
		if not(dia == 6):
			if (dia == 0):
				dia = "lunes"
			if (dia == 1):
				dia = "martes"
			if (dia == 2):
				dia = "miercoles"
			if (dia == 3):
				dia = "jueves"
			if (dia == 4):
				dia = "viernes"
			if (dia == 5):
				dia = "sabado"
			salon = salon + dia
			from universidad.universidad.utils import validate_overlap_for

			save = True

			save = validate_overlap_for(self, "Horario Curso", "profesor", self.profesor, dia, self.hora_posponer_inicio, self.hora_posponer_fin)
			save = validate_overlap_for(self, "Horario Curso", salon, self.salon_posponer, dia, self.hora_posponer_inicio, self.hora_posponer_fin)
			save = validate_overlap_for(self, "Horario Curso", "horario", self.horario, dia, self.hora_posponer_inicio, self.hora_posponer_fin)
		return save

