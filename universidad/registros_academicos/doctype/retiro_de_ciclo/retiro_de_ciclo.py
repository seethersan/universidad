# -*- coding: utf-8 -*-
# Copyright (c) 2015, seethersan and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe.model.document import Document
from universidad.registros_academicos.doctype.matricula.matricula import Matricula

class RetirodeCiclo(Matricula):
	def update_matricula(self):
		frappe.db.sql("""UPDATE `tabRegistro de Matricula`
			SET estado = 'Retirado'
			WHERE periodo = %s
			AND alumno = %s
			AND estado = 'En curso'""",
			(self.periodo,self.alumno))
