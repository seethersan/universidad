// Copyright (c) 2016, seethersan and contributors
// For license information, please see license.txt

frappe.provide("universidad.retiro_de_ciclo");
frappe.ui.form.on('Retiro de Ciclo', {
	refresh: function(frm) {
		frm.disable_save();
	}
});


frappe.ui.form.on('Retiro de Ciclo','periodo', function(frm){
	universidad.retiro_de_ciclo.check_mandatory_to_set_button(frm);
});


frappe.ui.form.on('Retiro de Ciclo','alumno', function(frm){
	frappe.call({
		method: "get_carrera_alumno",
		doc: frm.doc,
		callback: function (r) {
			var carrera = r.message;
			var carreras = [];
			for (var i in carrera){
				carreras.push(carrera[i]['carrera'])
			}
			cur_frm.set_query("carrera", function() {
		        return {
	        		"filters": [
		                ["name", "in", carreras]
	            	]
	        	};
	    	});
		}
	})
	universidad.retiro_de_ciclo.check_mandatory_to_set_button(frm);
});


frappe.ui.form.on('Retiro de Ciclo','carrera', function(frm){
	universidad.retiro_de_ciclo.check_mandatory_to_set_button(frm);
	frappe.call({
		method: "get_cursos_permitidos",
		doc: frm.doc,
		callback: function (r) {
			frm.fields_dict.cursos.grid.get_field('curso').get_query = function() {
		        return {
	        		"filters": [
		                ["curso", "in", r.message]
	            	]
	        	};
	    	};
		}
	})
});


universidad.retiro_de_ciclo.check_mandatory_to_set_button = function(frm) {
	if (frm.doc.periodo && frm.doc.alumno && frm.doc.carrera) {
		frappe.call({
			method: "get_matricula_name",
			doc: frm.doc,
			callback: function (r) {
				if (r.message) {
					frappe.call({
						method: "get_matricula",
						doc: frm.doc,
						callback: function (r) {
							if (r.message) {
								frm.set_value("cursos", r.message);
							}
						}
					})
					frm.fields_dict.retiro.$input.addClass("btn-primary");
				}
			}
		})
	}
	else {
		frm.fields_dict.retiro.$input.removeClass("btn-primary");
	}
};


frappe.ui.form.on('Retiro de Ciclo','retiro', function(frm){
	if (frm.doc.periodo && frm.doc.alumno && frm.doc.carrera) {
		frappe.call({
			method: "get_matricula_name",
			doc: frm.doc,
			callback: function (r) {
				if (r.message){
					frappe.call({
						method: "update_matricula",
						doc: frm.doc
					})
					frappe.throw("Alumno retirado del ciclo con éxito.");
				}
				else {
					frappe.throw("Alumno no matriculado.");
				}		
			}
		})
	}
	else {
		frappe.throw("Seleccione todos los campos.");
	}
});
