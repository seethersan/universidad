// Copyright (c) 2016, seethersan and contributors
// For license information, please see license.txt
frappe.provide("universidad.retiro_de_curso");
frappe.ui.form.on('Retiro de Curso', {
	refresh: function(frm) {
		frm.disable_save();
	}
});


frappe.ui.form.on('Retiro de Curso','periodo', function(frm){
	universidad.retiro_de_curso.check_mandatory_to_set_button(frm);
});


frappe.ui.form.on('Retiro de Curso','alumno', function(frm){
	frappe.call({
		method: "get_carrera_alumno",
		doc: frm.doc,
		callback: function (r) {
			var carrera = r.message;
			var carreras = [];
			for (var i in carrera){
				carreras.push(carrera[i]['carrera'])
			}
			cur_frm.set_query("carrera", function() {
		        return {
	        		"filters": [
		                ["name", "in", carreras]
	            	]
	        	};
	    	});
		}
	})
	universidad.retiro_de_curso.check_mandatory_to_set_button(frm);
});