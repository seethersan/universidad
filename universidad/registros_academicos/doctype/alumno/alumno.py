# -*- coding: utf-8 -*-
# Copyright (c) 2015, seethersan and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe.model.document import Document
from universidad.universidad.utils import Utils

class Alumno(Utils):
	def get_customer(self):
		cliente = frappe.db.sql("""select name as name 
				from `tabCustomer` where codigo = %s """, 
				self.codigo_alumno, as_dict=1)
		return cliente


	def get_sales_order(self):
		sales_order = frappe.db.sql("""select sales.name as name 
				from `tabSales Order` sales,
				`tabSales Order Item` item 
				where item.parent = sales.name
				AND sales.tax_id = %s
				AND item.item_name = %s """, 
				(self.numero_documento, "Matricula"), as_dict=1)
		return sales_order


	def set_codigo(self):
		if not self.codigo_alumno:
			count = frappe.db.sql("""select (count(name) + 1) as cuenta
					from `tabAlumno` where YEAR(creation)= %s""",
					date.today().year)
			cuenta = count[0]
			if date.today().month < 6:
				periodo = "01"
			else:
				periodo = "02"
			codigo = str(date.today().year) + periodo + str(cuenta[0]).zfill(4)
		else:
			codigo = self.codigo_alumno 
		return codigo


