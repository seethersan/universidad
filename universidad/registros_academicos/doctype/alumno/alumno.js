// Copyright (c) 2016, seethersan and contributors
// For license information, please see license.txt

// Copyright (c) 2016, seethersan and contributors
// For license information, please see license.txt
frappe.provide("universidad.alumno");
frappe.ui.form.on('Alumno', {
	refresh: function(frm) {
	}
});


frappe.ui.form.on('Alumno', {
	validate: function(frm) {
	var today = new Date();		
		frm.set_value("estado", "Alumno Pre-Inscrito");
		frappe.call({
			method: "get_customer",
			doc: frm.doc,
			callback: function (r) {
				if (r.message) {

				}
				else {
					frappe.call({
						method: "frappe.client.insert",
						args: {
							doc: {
								doctype: "Customer",
								customer_name: frm.doc.nombre_completo,
								categoria: "Alumno Pre-Inscrito",
								codigo: frm.doc.codigo_alumno,
								customer_type: "Individual",
								customer_group: "Individual",
								territory: "Peru",
								tipo_documento_identidad: frm.doc.tipo_documento,
								tax_id: frm.doc.numero_documento,
								codigo_tipo_documento: frm.doc.codigo_tipo_documento
							}
						}
					});
				}
			}
		});
		frappe.call({
			method: "get_sales_order",
			doc: frm.doc,
			callback: function (r) {
				if (r.message) {

				}
				else {
					frappe.call({
						method: "frappe.client.insert",
						args: {
							doc: {
								doctype: "Sales Order",
								customer: frm.doc.nombre_completo,
								tax_id: frm.doc.numero_documento,
								codigo_tipo_documento: frm.doc.codigo_tipo_documento,
								delivery_date: today,
								order_type: "Sales",
								docstatus: 1,
								status: "To Deliver and Bill",
								items: [{
									item_code: "Matricula",
									item_name: "Matricula",
									description: "Matricula",
									qty: 1,
									rate: frm.doc.costo_examen
								}]
							}
						}
					})
				}
			}
		});
		if(frm.doc.requisitos){
			var values = frm.doc.requisitos
			frm.set_value("requisitos", values)
		}
	}
});


frappe.ui.form.on("Alumno", {
	tipo_documento: function(frm, cdt, cdn) {
		var tipo_documento = frappe.model.get_doc(cdt, cdn);
		if (tipo_documento.tipo_documento){
			frappe.call({
				type: "GET",
				method: "ple.ple_peru.doctype.tipos_de_documento_de_identidad.tipos_de_documento_de_identidad.get_tipo_documento",
				args: {
					tipo_documento_identidad: tipo_documento.tipo_documento
				},
				callback: function(r) {
					frappe.model.set_value(cdt, cdn, "codigo_tipo_documento", r.message);
				}
			});
		}
		else {
			frappe.model.set_value(cdt, cdn, "codigo_tipo_documento", null);
		}
	}
});


frappe.ui.form.on('Alumno', 'apellido_paterno', function(frm) {
	universidad.alumno.check_mandatory_to_set_nombre_completo(frm);
	universidad.alumno.check_mandatory_to_set_codigo(frm);
});


frappe.ui.form.on('Alumno', 'apellido_materno', function(frm) {
	universidad.alumno.check_mandatory_to_set_nombre_completo(frm);
	universidad.alumno.check_mandatory_to_set_codigo(frm);
});


frappe.ui.form.on('Alumno', 'nombres', function(frm) {
	universidad.alumno.check_mandatory_to_set_nombre_completo(frm);
	universidad.alumno.check_mandatory_to_set_codigo(frm);
});


universidad.alumno.check_mandatory_to_set_codigo = function(frm) {
	if (frm.doc.apellido_paterno && frm.doc.apellido_materno && frm.doc.nombres) {
		universidad.alumno.get_codigo(frm);
	}
}

universidad.alumno.check_mandatory_to_set_nombre_completo = function(frm) {
	if (frm.doc.apellido_paterno && frm.doc.apellido_materno && frm.doc.nombres) {
		universidad.alumno.set_nombre_completo(frm);
	}
}

universidad.alumno.get_codigo = function(frm) {
	if (frm.doc.proceso_admision) {
		frappe.call({
				method: "set_codigo",
				doc: frm.doc,
				callback: function(r) {
					if(r.message) {
						frm.set_value("codigo_alumno", r.message);
					}
				}
			})
	}
}

universidad.alumno.set_nombre_completo = function(frm) {
	if (frm.doc.apellido_paterno && frm.doc.apellido_materno && frm.doc.nombres) {
		frm.set_value("nombre_completo", frm.doc.apellido_paterno + " " + frm.doc.apellido_materno + " " + frm.doc.nombres)
	}
}


