// Copyright (c) 2016, seethersan and contributors
// For license information, please see license.txt
frappe.provide("universidad.rectificacion_de_matricula");
frappe.ui.form.on('Rectificacion de Matricula', {
	refresh: function(frm) {
		frm.disable_save();
	}
});


frappe.ui.form.on('Rectificacion de Matricula','periodo', function(frm){
	universidad.rectificacion_de_matricula.check_mandatory_to_set_button(frm);
});


frappe.ui.form.on('Rectificacion de Matricula','alumno', function(frm){
	frappe.call({
		method: "get_carrera_alumno",
		doc: frm.doc,
		callback: function (r) {
			var carrera = r.message;
			var carreras = [];
			for (var i in carrera){
				carreras.push(carrera[i]['carrera'])
			}
			cur_frm.set_query("carrera", function() {
		        return {
	        		"filters": [
		                ["name", "in", carreras]
	            	]
	        	};
	    	});
		}
	})
	universidad.rectificacion_de_matricula.check_mandatory_to_set_button(frm);
});


frappe.ui.form.on('Rectificacion de Matricula','carrera', function(frm){
	universidad.rectificacion_de_matricula.check_mandatory_to_set_button(frm);
	frappe.call({
		method: "get_cursos_permitidos",
		doc: frm.doc,
		callback: function (r) {
			frm.fields_dict.cursos.grid.get_field('curso').get_query = function() {
		        return {
	        		"filters": [
		                ["curso", "in", r.message]
	            	]
	        	};
	    	};
		}
	})
});


var cursos_elegidos = [];


frappe.ui.form.on('Detalle Matricula Cursos', 'curso', function(frm, cdt, cdn){
	var row = frappe.get_doc(cdt, cdn);
	var curso = row['curso']
	if (curso) {	
		if (cursos_elegidos.indexOf(curso) < 0){
			frappe.call({
				method: "get_datos_salon",
				doc: frm.doc,
				args: {
					horario: curso
				},
				callback: function (r) {
					datos = r.message[0]
					if (datos["alumnos"] < datos["capacidad"]){
						frappe.model.set_value(cdt, cdn, "alumnos", datos["alumnos"]);
						frappe.model.set_value(cdt, cdn, "capacidad", datos["capacidad"]);
						frappe.model.set_value(cdt, cdn, "estado", datos["estado"]);
						frappe.call({
							method: "validate_overlap",
							doc: frm.doc,
							args: {
								horario: curso
							},
							callback: function (r) {
								if (r.message) {

								}
								else {
									cursos_elegidos.splice(cursos_elegidos.indexOf(curso),1);
									cur_frm.fields_dict.cursos.grid.grid_rows[cur_frm.fields_dict.cursos.grid.grid_rows.length - 1].remove();
								}
							}
						});
					}
					else {
						cursos_elegidos.splice(cursos_elegidos.indexOf(curso),1);
						cur_frm.fields_dict.cursos.grid.grid_rows[cur_frm.fields_dict.cursos.grid.grid_rows.length - 1].remove();
					}					
				}
			});
			cursos_elegidos.push(curso);
		}
		else {
			cursos = frm.doc.cursos;
			for (i in cursos){
				cursos_seleccionados.push(cursos[i]["curso"])
			}
			if (curso && cursos_seleccionados.indexOf(curso) < 0) {
				cursos_elegidos.splice(cursos_elegidos.indexOf(curso),1);
			}
			cur_frm.fields_dict.cursos.grid.grid_rows[cur_frm.fields_dict.cursos.grid.grid_rows.length - 1].remove();
		}		
	}
	else {
		frappe.model.set_value(cdt, cdn, "alumnos", "");
		frappe.model.set_value(cdt, cdn, "capacidad", "");
		frappe.model.set_value(cdt, cdn, "estado", "");
		if (curso) {
			cursos_elegidos.splice(cursos_elegidos.indexOf(curso),1);
		}
	}
})


universidad.rectificacion_de_matricula.check_mandatory_to_set_button = function(frm) {
	if (frm.doc.periodo && frm.doc.alumno && frm.doc.carrera) {
		frappe.call({
			method: "get_matricula_name",
			doc: frm.doc,
			callback: function (r) {
				if (r.message) {
					frappe.call({
						method: "get_matricula",
						doc: frm.doc,
						callback: function (r) {
							if (r.message) {
								frm.set_value("cursos", r.message);
							}
						}
					})
					frm.fields_dict.rectificar.$input.addClass("btn-primary");
				}
			}
		})
	}
	else {
		frm.fields_dict.rectificar.$input.removeClass("btn-primary");
	}
};


frappe.ui.form.on('Rectificacion de Matricula','rectificar', function(frm){
	if (frm.doc.periodo && frm.doc.alumno && frm.doc.carrera) {
		frappe.call({
			method: "get_matricula_name",
			doc: frm.doc,
			callback: function (r) {
				if (r.message){
					frappe.call({
						method: "delete_matricula",
						doc: frm.doc,
						callback: function (r) {
							var cursos = [];
							var vez = 0;
							cursos = frm.doc.cursos
							for (i in cursos) {
								frappe.call({
									method: "get_veces_curso",
									doc: frm.doc,
									args: {
										curso: cursos[i]['curso']
									},
									callback: function (r) {
										dato = r.message[0]
										vez = dato['veces'] + 1
										curso = r.message[1]
										frappe.call({
											method: "frappe.client.insert",
											args: {
												doc: {
													doctype: "Registro de Matricula",
													periodo: frm.doc.periodo,
													alumno: frm.doc.alumno,
													curso: curso,
													veces: vez
												}
											}
										});
									}
								})						
							}
						}
					})
				}
				else {
					frappe.throw("Alumno no matriculado.");
				}		
			}
		})
	}
	else {
		frappe.throw("Seleccione todos los campos.");
	}
});
