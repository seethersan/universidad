# -*- coding: utf-8 -*-
# Copyright (c) 2015, seethersan and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe.model.document import Document
from universidad.registros_academicos.doctype.matricula.matricula import Matricula

class RectificaciondeMatricula(Matricula):
	def delete_matricula(self):
		frappe.db.sql("""delete from `tabRegistro de Matricula`
			where periodo = %s
			and alumno = %s""",
			(self.periodo,self.alumno))
