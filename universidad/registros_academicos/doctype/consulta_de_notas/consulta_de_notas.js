// Copyright (c) 2016, seethersan and contributors
// For license information, please see license.txt
frappe.provide("universidad.consulta_de_notas");
frappe.ui.form.on('Consulta de Notas', {
	refresh: function(frm) {
		frm.disable_save();
		frm.hide_print();
		
	}
});


frappe.ui.form.on('Consulta de Notas','alumno', function(frm){
	universidad.consulta_de_notas.check_mandatory_to_set_button(frm);
	if (frm.doc.alumno) {
		frappe.call({
			method: "get_carrera_alumno",
			doc: frm.doc,
			callback: function (r) {
				var carrera = r.message;
				var carreras = [];
				for (var i in carrera){
					carreras.push(carrera[i]['carrera'])
				}
				cur_frm.set_query("carrera", function() {
			        return {
		        		"filters": [
			                ["name", "in", carreras]
		            	]
		        	};
		    	});
			}
		})
	}
});


frappe.ui.form.on('Consulta de Notas','carrera', function(frm){
	universidad.consulta_de_notas.check_mandatory_to_set_button(frm);
});


frappe.ui.form.on('Consulta de Notas','periodo', function(frm){
	universidad.consulta_de_notas.check_mandatory_to_set_button(frm);
});


frappe.ui.form.on('Consulta de Notas','notas', function(frm){
	universidad.consulta_de_notas.check_mandatory_to_set_button(frm);
});


universidad.consulta_de_notas.check_mandatory_to_set_button = function(frm) {
	if (frm.doc.carrera && frm.doc.periodo && frm.doc.alumno) {	
		frm.fields_dict.consulta.$input.addClass("btn-primary");
		if (frm.doc.notas.length === 0) {
			frm.fields_dict.imprimir.$input.removeClass("btn-primary");

		}
		else {
			frm.fields_dict.imprimir.$input.addClass("btn-primary");
		}
	}
	else {
		frm.fields_dict.consulta.$input.removeClass("btn-primary");
		frm.fields_dict.imprimir.$input.removeClass("btn-primary");
	}
};


universidad.consulta_de_notas.get_or_create_consulta = function(frm) {
	frappe.call({
		method: "get_constancia",
		doc: frm.doc,
		callback: function (r) {
			if (r.message) {
				consulta = r.message
				universidad.consulta_de_notas.get_constancia(frm);
			}
			else {
				frappe.call({
					method: "get_alumno_nombre",
					doc: frm.doc,
					callback: function (r) {
						if (r.message) {
							alumno = r.message
							frappe.call({
								method: "frappe.client.insert",
								args: {
									doc: {
										doctype: "Constancia",
										alumno: alumno[0]["nombre"],
										codigo: alumno[0]["codigo"],
										carrera: frm.doc.carrera,
										periodo: frm.doc.periodo,
										notas: frm.doc.notas
									}
								},
								callback: function (r) {
									consulta = r.message
									universidad.consulta_de_notas.get_constancia(frm);
								}
							});
						}
					}
				});
			}		
		}
	})
};


universidad.consulta_de_notas.get_constancia = function(frm) {
	frappe.call({
		method: "get_constancia",
		doc: frm.doc,
		callback: function (r) {
			if (r.message) {
				consulta = r.message
				frm.set_value("constancia", consulta[0]["name"])
			}		
		}
	})
};


frappe.ui.form.on('Consulta de Notas','consulta', function(frm){
	if (frm.doc.carrera && frm.doc.periodo) {	
		frappe.call({
			method: "get_notas_alumno",
			doc: frm.doc,
			callback: function (r) {
				if (r.message) {
					frm.set_value("notas", r.message)
					universidad.consulta_de_notas.check_mandatory_to_set_button(frm);
					universidad.consulta_de_notas.get_or_create_consulta(frm);
				}
				else {
					frappe.throw("Alumno no estudió este periodo");
				}
			}
		});
		
	}
	else {
		frappe.throw("Seleccione su Carrera y el Periodo Académico");
	}
});


frappe.ui.form.on('Consulta de Notas','imprimir', function(frm){
	if (frm.doc.carrera && frm.doc.periodo && frm.doc.constancia && frm.doc.alumno) {
		var w = window.open(
			frappe.urllib.get_full_url("/api/method/frappe.utils.print_format.download_pdf?"
			+"doctype="+encodeURIComponent("Constancia")
			+"&name="+encodeURIComponent(frm.doc.constancia)
			+"&format=Standard"
			+"&no_letterhead=0"
			+(this.lang_code ? ("&_lang="+this.lang_code) : "")));
		if(!w) {
			msgprint(__("Please enable pop-ups")); return;
		}
	}
	else {
		frappe.throw("Seleccione su Carrera y el Periodo Académico");
	}
});
