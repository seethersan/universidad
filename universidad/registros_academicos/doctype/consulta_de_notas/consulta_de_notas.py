# -*- coding: utf-8 -*-
# Copyright (c) 2015, seethersan and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe.model.document import Document
from universidad.universidad.utils import Utils

class ConsultadeNotas(Utils):
	def get_carrera_alumno(self):
		carrera = frappe.db.sql("""SELECT de.carrera as carrera 
					FROM `tabAlumno` as al,`tabDetalle Carreras Alumno` as de
					WHERE de.parent = al.nombre_completo
					AND al.nombre_completo = %s""",
					(self.alumno), as_dict=1)
		return carrera


	def get_alumno_nombre(self):
		nombre = frappe.db.sql("""SELECT name as nombre, codigo_alumno as codigo 
					FROM `tabAlumno`
					WHERE nombre_completo = %s""",
					(self.alumno), as_dict=1)
		return nombre


	def get_notas_alumno(self):
		notas = frappe.db.sql("""SELECT nota.curso as curso, cu.numero_creditos as credito, nota.nota as nota, IF(nota.nota < 11,"DESAPROBADO", "APROBADO") as estado
					FROM `tabResultado Notas Promedio` as nota, `tabCursos` as cu
					WHERE cu.nombre_curso = nota.curso
					AND nota.alumno = %s
					AND nota.periodo = %s""",
					(self.alumno,self.periodo), as_dict=1)
		return notas


	def get_constancia(self):
		nombre = self.get_alumno_nombre()
		constancia = frappe.db.sql("""SELECT *
					FROM `tabConstancia`
					WHERE alumno = %s
					AND carrera = %s
					AND periodo = %s""",
					(self.alumno,self.carrera,self.periodo), as_dict=1)
		return constancia