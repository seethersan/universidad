# -*- coding: utf-8 -*-
# Copyright (c) 2015, seethersan and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe.model.document import Document
from universidad.universidad.utils import Utils, OverlapError

class Matricula(Utils):
	def get_matricula(self):
		matricula = frappe.db.sql("""SELECT ma.curso as curso, (SELECT count(alumno) as alumno FROM `tabRegistro de Matricula`
                    where curso = ma.curso) as alumnos, cu.capacidad as capacidad, IF((SELECT count(alumno) as alumno FROM `tabRegistro de Matricula`
                    where curso = ma.curso)<cu.capacidad,'Disponible','Lleno') as estado
					from `tabRegistro de Matricula` as ma, `tabHorario Curso` as cu where cu.name = ma.curso
					and periodo = %s
					and alumno = %s
					and estado = 'En curso'
					GROUP BY ma.curso""", 
					(self.periodo, self.alumno), as_dict=1)
		return matricula


	def get_matricula_name(self):
		matricula = frappe.db.sql("""select ma.name as name, ma.curso as curso
					from `tabRegistro de Matricula` as ma, `tabHorario Curso` as cu where cu.name = ma.curso
					and periodo = %s
					and alumno = %s""", 
					(self.periodo, self.alumno), as_dict=1)
		return matricula


	def get_veces_curso(self, curso=None):
		veces = frappe.db.sql("""select count(name) as veces
					from `tabRegistro de Matricula` where curso = %s 
					and alumno = %s
					and estado = 'Reprobado'""", 
					(curso, self.alumno), as_dict=1)
		return veces, curso


	def get_carrera_alumno(self):
		carrera = frappe.db.sql("""SELECT de.carrera as carrera 
					FROM `tabAlumno` as al,`tabDetalle Carreras Alumno` as de
					WHERE de.parent = al.nombre_completo
					AND al.nombre_completo = %s""",
					(self.alumno), as_dict=1)
		return carrera


	def get_cursos_permitidos(self):
		cursos_aprobados = frappe.db.sql("""select curso as curso
					from `tabRegistro de Matricula` where alumno = %s
					and estado = 'Aprobado'
					and carrera = %s""",
					(self.alumno, self.carrera), as_dict=1)
		cursos_disponibles = frappe.db.sql("""SELECT cu.name as curso, re.curso as pre_requisito
					FROM `tabCursos` as cu
					LEFT JOIN`tabCursos Pre Requisitos` as re
					ON cu.name = re.parent
					WHERE cu.carrera = %s""",
					(self.carrera), as_dict=1)
		aprobados = []
		cursos_permitidos = []
		for i in range(len(cursos_aprobados)):
			aprobados.append(cursos_aprobados[i]["curso"]) 
		for i in range(len(cursos_disponibles)):
			if cursos_disponibles[i]["curso"] in cursos_permitidos:
				if not cursos_disponibles[i]["pre_requisito"] in cursos_aprobados:
					cursos_permitidos.remove(cursos_disponibles[i]["curso"])
			else:
				if cursos_disponibles[i]["pre_requisito"] in cursos_aprobados:
					cursos_permitidos.append(cursos_disponibles[i]["curso"])
				elif cursos_disponibles[i]["pre_requisito"] == None:
					cursos_permitidos.append(cursos_disponibles[i]["curso"])
		return cursos_permitidos

	def get_datos_salon(self, horario):
		capacidad = frappe.db.sql("""SELECT count(ma.alumno) as alumnos, cu.capacidad as capacidad, IF(count(ma.alumno)<cu.capacidad,'Disponible','Lleno') as estado FROM 
					`tabRegistro de Matricula` as ma, `tabHorario Curso`as cu
					WHERE cu.name = ma.curso
					AND ma.curso = %s""",
					(horario), as_dict=1)
		return capacidad


	def validate_overlap(self, horario):
		"""Validates overlap for Student Group, Instructor, Room"""
		save = True
		cursos = "('"
		i = 1
		
		from universidad.universidad.utils import validate_overlap_for_horario
		for curso in self.cursos:
			cursos = cursos + curso.curso
			if not (i == len(self.cursos)):
				cursos = cursos + "','"
			i = i + 1
		cursos = cursos + "')"
		horario_curso = frappe.db.sql("""SELECT * from `tabHorario Curso` WHERE name = %s""",
				(horario), as_dict=1)
		horario_curso = horario_curso[0]
		if horario_curso['lunes']:
			save = validate_overlap_for_horario(self, "Horario Curso", "horario", horario_curso['name'], "lunes", horario_curso['inicio_lunes'], horario_curso['salida_lunes'], horario_curso['periodo_academico'], cursos)
		if horario_curso['martes']:
			save = validate_overlap_for_horario(self, "Horario Curso", "horario", horario_curso['name'], "martes", horario_curso['inicio_martes'], horario_curso['salida_martes'], horario_curso['periodo_academico'], cursos)
		if horario_curso['miercoles']:
			save = validate_overlap_for_horario(self, "Horario Curso", "horario", horario_curso['name'], "miercoles", horario_curso['inicio_miercoles'], horario_curso['salida_miercoles'], horario_curso['periodo_academico'], cursos)
		if horario_curso['jueves']:
			save = validate_overlap_for_horario(self, "Horario Curso", "horario", horario_curso['name'], "jueves", horario_curso['inicio_jueves'], horario_curso['salida_jueves'], horario_curso['periodo_academico'], cursos)
		if horario_curso['viernes']:
			save = validate_overlap_for_horario(self, "Horario Curso", "horario", horario_curso['name'], "viernes", horario_curso['inicio_viernes'], horario_curso['salida_viernes'], horario_curso['periodo_academico'], cursos)
		if horario_curso['sabado']:
			save = validate_overlap_for_horario(self, "Horario Curso", "horario", horario_curso['name'], "sabado", horario_curso['inicio_sabado'], horario_curso['salida_sabado'], horario_curso['periodo_academico'], cursos)
		return save
