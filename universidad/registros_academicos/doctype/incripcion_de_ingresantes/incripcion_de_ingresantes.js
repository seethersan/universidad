// Copyright (c) 2016, seethersan and contributors
// For license information, please see license.txt
frappe.provide("universidad.inscripcion_de_ingresantes");
frappe.ui.form.on('Incripcion de Ingresantes', {

	refresh: function(frm) {
		frm.disable_save();
		frappe.call({
			method: "get_procesos_inscripcion",
			doc: frm.doc,
			callback: function (r) {
				var proceso = r.message;
				var procesos = [];
				for (var i in proceso){
					procesos.push(proceso[i]['codigo_proceso'])
				}
				cur_frm.set_query("proceso_admision", function() {
			        return {
		        		"filters": [
			                ["name", "in", procesos]
		            	]
		        	};
		    	});
			}
		})
	}
});

cur_frm.add_fetch('postulante','nombre_completo','nombre');


frappe.ui.form.on('Incripcion de Ingresantes', 'examen', function(frm) {
	universidad.inscripcion_de_ingresantes.check_mandatory_to_set_button(frm);
	
});


frappe.ui.form.on('Incripcion de Ingresantes', 'postulante', function(frm) {
	universidad.inscripcion_de_ingresantes.check_mandatory_to_set_button(frm);
	
});


frappe.ui.form.on('Incripcion de Ingresantes', 'evaluacion_pastoral', function(frm) {
	universidad.inscripcion_de_ingresantes.check_mandatory_to_set_button(frm);
	
});

universidad.inscripcion_de_ingresantes.check_mandatory_to_set_button = function (frm) {
	if (frm.doc.examen && frm.doc.postulante && frm.doc.evaluacion_pastoral) {
		frm.fields_dict.registrar.$input.addClass("btn-primary");
	}
	else {
		frm.fields_dict.registrar.$input.removeClass("btn-primary");
	}
}


frappe.ui.form.on('Incripcion de Ingresantes', 'registrar', function(frm) {
	if (frm.doc.examen && frm.doc.postulante && frm.doc.evaluacion_pastoral) {
		frappe.call({
			method: "get_alumno",
			doc: frm.doc,
			callback: function (r) {
				if (r.message) {
					frappe.throw("Postulante ya inscrito")
				}
				else {
					frappe.call({
						method: "get_datos_postulante",
						doc: frm.doc,
						callback: function (r) {
							postulante = r.message[0]
							codigo_postulante = postulante["codigo_postulante"]
							postulante["estado"] = "Alumno Inscrito"
							delete postulante["codigo_postulante"]
							delete postulante["owner"]
							delete postulante["parentfield"]
							delete postulante["parenttype"]
							delete postulante["idx"]
							delete postulante["docstatus"]
							delete postulante["data_22"]
							delete postulante["costo_examen"]
							delete postulante["_assign"]
							delete postulante["_comments"]
							delete postulante["_liked_by"]
							delete postulante["_user_tags"]
							delete postulante["parent"]
							delete postulante["creation"]
							delete postulante["modified_by"]
							delete postulante["name"]
							delete postulante["modified"]
							delete postulante["requisitos"]
							postulante["codigo_alumno"] = codigo_postulante
							postulante["doctype"] = "Alumno"
							carrera = postulante["carrera"]
							postulante["carrera"] = [{carrera}]
							for (var i in postulante) {
								if (postulante[i] == null){
									delete postulante[i]
								}
							}
							console.log(postulante)
							frappe.call({
								method: "frappe.client.insert",
								args: {
									doc: postulante
								}
							});
						}
					})
				}
			}
		})
	}
	else {
		frm.fields_dict.registrar.$input.removeClass("btn-primary");
	}
	
});

