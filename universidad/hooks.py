# -*- coding: utf-8 -*-
from __future__ import unicode_literals

app_name = "universidad"
app_title = "Universidad"
app_publisher = "seethersan"
app_description = "Modulo para la gestion de una Universidad"
app_icon = "octicon octicon-file-directory"
app_color = "blue"
app_email = "carlos_jcez@hotmail.com"
app_version = "0.0.1"
app_license = "MIT"

# Includes in <head>
# ------------------

# include js, css files in header of desk.html
# app_include_css = "/assets/universidad/css/universidad.css"
# app_include_js = "/assets/universidad/js/universidad.js"

# include js, css files in header of web template
# web_include_css = "/assets/universidad/css/universidad.css"
# web_include_js = "/assets/universidad/js/universidad.js"

# Home Pages
# ----------

# application home page (will override Website Settings)
# home_page = "login"

# website user home page (by Role)
# role_home_page = {
#	"Role": "home_page"
# }

# Website user home page (by function)
# get_website_user_home_page = "universidad.utils.get_home_page"

# Generators
# ----------

# automatically create page for each record of this doctype
# website_generators = ["Web Page"]

# Installation
# ------------

# before_install = "universidad.install.before_install"
# after_install = "universidad.install.after_install"

# Desk Notifications
# ------------------
# See frappe.core.notifications.get_notification_config

# notification_config = "universidad.notifications.get_notification_config"

# Permissions
# -----------
# Permissions evaluated in scripted ways

# permission_query_conditions = {
# 	"Event": "frappe.desk.doctype.event.event.get_permission_query_conditions",
# }
#
# has_permission = {
# 	"Event": "frappe.desk.doctype.event.event.has_permission",
# }

# Document Events
# ---------------
# Hook on document methods and events

# doc_events = {
# 	"*": {
# 		"on_update": "method",
# 		"on_cancel": "method",
# 		"on_trash": "method"
#	}
# }

# Scheduled Tasks
# ---------------

# scheduler_events = {
# 	"all": [
# 		"universidad.tasks.all"
# 	],
# 	"daily": [
# 		"universidad.tasks.daily"
# 	],
# 	"hourly": [
# 		"universidad.tasks.hourly"
# 	],
# 	"weekly": [
# 		"universidad.tasks.weekly"
# 	]
# 	"monthly": [
# 		"universidad.tasks.monthly"
# 	]
# }

# Testing
# -------

# before_tests = "universidad.install.before_tests"

# Overriding Whitelisted Methods
# ------------------------------
#
# override_whitelisted_methods = {
# 	"frappe.desk.doctype.event.event.get_events": "universidad.event.get_events"
# }

